package RestAPITest;

import static com.jayway.restassured.RestAssured.get;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;


public class AGNcreatePOST {
	//@Test
    public static void main(String[] args) throws IOException, JSONException {
		
		//make get request to fetch capital of norway
		String resp = "http://dev-godam.delhivery.com/datasync/api/agn/";
		
		String APIBody = "{\"load\":[{"
	            + "\"source_number\": \"AGN095\","
	            + "\"auxilliary_vendor\": ,"
	            + "\"facility_num\": \"supplier666\","
	            + "\"prod_details\":[{"  
	                   + "\"prod_number\": \"A66681\","
	                   + "\"qty\": 1,"
	        +"},"
	        + "{"
	            + "\"source_number\": \"AGN095\","
	            + "\"auxilliary_vendor\": ,"
	            + "\"facility_num\": \"supplier666\","
	            + "\"prod_details\": [{ "
	                   + "\"prod_number\": \"A66681\","
	                   + "\"qty\": 1,"
	            +"}]}";
                
	  // Building request using requestSpecBuilder
	    RequestSpecBuilder builder = new RequestSpecBuilder();
		
	  //Setting API's body 
	    builder.setBody(APIBody); 

	  //Setting content type as application/json or application/xml 
	    
	  // builder.setContentType("application/json; charset=UTF-8"); 
	  RequestSpecification requestSpec = builder.build();

	  String Token =  "2a057188bda74e66bbe0b7840ca899b38c541b91";
	    
	    //Making post request with authentication, leave blank in case there are no credentials- basic("","") 
	   // Response response = given().authentication().preemptive().basic("","").spec(requestSpec).when().post(resp);
	    Response response = given().authentication().preemptive().basic(Token,"") .spec(requestSpec).when().post(resp);
	    
	    JSONObject JSONResponseBody = new JSONObject(response.body().asString());
	    
	    //Fetching the desired value of a parameter 
	    String result = JSONResponseBody.getString("message");

	    System.out.println(result);
	    
	     //Asserting that result of Norway is Oslo 
	    Assert.assertEquals(result, "{DataSync has been started. Check run log with id +{}}");
	}

	private static RequestSpecification given() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
