package RestApiTest;

import javax.ws.rs.POST;

import org.openqa.selenium.remote.JsonException;
import org.testng.annotations.Test;

 
public class RestTest<RequestSpecification, RequestSpecBuilder> {
	@POST
	public void httpPost() throws JsonException,InterruptedException 
	{ 

	//Initializing Rest API's URL 
	String APIUrl = "http://dev-godam.delhivery.com/datasync/api/agn/";
	
	String str = request.getParameter("FCDEL1");
	
	@Path("/http://dev-godam.delhivery.com/datasync/api/agn//{FCDEL1}}")
	public void create(@PathParam("fulfillment_center") String FCDEL1,
	                   	
	//Initializing payload or API body 
	String APIBody = "{\"load\":[{"
            + "\"source_number\": \"AGN095\","
            + "\"auxilliary_vendor\": ,"
            + "\"facility_num\": \"supplier666\","
            + "\"prod_details\":[{"  
                   + "\"prod_number\": \"A66681\","
                   + "\"qty\": 1,"
        +"},"
        + "{"
            + "\"source_number\": \"AGN095\","
            + "\"auxilliary_vendor\": ,"
            + "\"facility_num\": \"supplier666\","
            + "\"prod_details\": [{ "
                   + "\"prod_number\": \"A66681\","
                   + "\"qty\": 1,"
            +"}]}";
	
	// Building request using requestSpecBuilder 
	RequestSpecBuilder builder = new RequestSpecBuilder; 
	
	//Setting API's body 
	builder.setBody(APIBody); 

	//Setting content type as application/json or application/xml 
	builder.setContentType("application/json; charset=UTF-8"); 
	builder.setAccept("application/json");
	builder.setAuthorization("Token 61c413caf8da2df2b2c0713fc5e4dd3c4c62c157");

	RequestSpecification requestSpec = builder.build(); 

	//Making post request with authentication, leave blank in case there are no credentials- basic("","") 
	Response response = given().authentication().preemptive().basic({username}, {password}) .spec(requestSpec).when().post(APIUrl); 
	JSONObject JSONResponseBody = new JSONObject(response.body().asString()); 

	//Fetching the desired value of a parameter 
	String result = JSONResponseBody.getString({key}); 

	//Asserting that result of Norway is Oslo 
	Assert.assertEquals(result, "{expectedValue}"); 
	} 
}
