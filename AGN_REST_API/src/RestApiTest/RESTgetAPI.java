package RestApiTest;
import static com.jayway.restassured.RestAssured.get;

import org.json.JSONArray;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.restassured.response.Response;

public class RESTgetAPI {
	@Test
	public static void main(String[] args) throws JSONException{	
		
		//make get request to fetch capital of norway
		Response resp = get("http://dev-godam.delhivery.com/po/api/agn/");

		//String Token =  "2a057188bda74e66bbe0b7840ca899b38c541b91";
		
		//Fetching response in JSON
		JSONArray jsonResponse = new JSONArray(resp.asString());

		//Fetching value of results parameter
		String result = jsonResponse.getJSONObject(0).getString("results");
		System.out.println(result);

		//Asserting that result of GET method
		Assert.assertEquals(result, 200, "status was 200 OK");
	}
		

}
