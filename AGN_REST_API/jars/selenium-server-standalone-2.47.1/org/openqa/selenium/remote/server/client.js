var e, m = this;
function n(a) {
  return void 0 !== a;
}
function q() {
}
function aa(a) {
  a.Ea = function() {
    return a.Kc ? a.Kc : a.Kc = new a;
  };
}
function ba(a) {
  var b = typeof a;
  if ("object" == b) {
    if (a) {
      if (a instanceof Array) {
        return "array";
      }
      if (a instanceof Object) {
        return b;
      }
      var c = Object.prototype.toString.call(a);
      if ("[object Window]" == c) {
        return "object";
      }
      if ("[object Array]" == c || "number" == typeof a.length && "undefined" != typeof a.splice && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("splice")) {
        return "array";
      }
      if ("[object Function]" == c || "undefined" != typeof a.call && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("call")) {
        return "function";
      }
    } else {
      return "null";
    }
  } else {
    if ("function" == b && "undefined" == typeof a.call) {
      return "object";
    }
  }
  return b;
}
function t(a) {
  return "array" == ba(a);
}
function ca(a) {
  var b = ba(a);
  return "array" == b || "object" == b && "number" == typeof a.length;
}
function v(a) {
  return "string" == typeof a;
}
function w(a) {
  return "function" == ba(a);
}
function da(a) {
  var b = typeof a;
  return "object" == b && null != a || "function" == b;
}
function x(a) {
  return a[ea] || (a[ea] = ++fa);
}
var ea = "closure_uid_" + (1E9 * Math.random() >>> 0), fa = 0;
function ga(a, b, c) {
  return a.call.apply(a.bind, arguments);
}
function ha(a, b, c) {
  if (!a) {
    throw Error();
  }
  if (2 < arguments.length) {
    var d = Array.prototype.slice.call(arguments, 2);
    return function() {
      var c = Array.prototype.slice.call(arguments);
      Array.prototype.unshift.apply(c, d);
      return a.apply(b, c);
    };
  }
  return function() {
    return a.apply(b, arguments);
  };
}
function y(a, b, c) {
  y = Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? ga : ha;
  return y.apply(null, arguments);
}
function ia(a, b) {
  var c = Array.prototype.slice.call(arguments, 1);
  return function() {
    var b = c.slice();
    b.push.apply(b, arguments);
    return a.apply(this, b);
  };
}
var ja = Date.now || function() {
  return +new Date;
};
function z(a, b) {
  function c() {
  }
  c.prototype = b.prototype;
  a.m = b.prototype;
  a.prototype = new c;
  a.prototype.constructor = a;
  a.Zd = function(a, c, g) {
    for (var h = Array(arguments.length - 2), k = 2;k < arguments.length;k++) {
      h[k - 2] = arguments[k];
    }
    return b.prototype[c].apply(a, h);
  };
}
;function la(a, b) {
  this.code = a;
  this.state = A[a] || ma;
  this.message = b || "";
  var c = this.state.replace(/((?:^|\s+)[a-z])/g, function(a) {
    return a.toUpperCase().replace(/^[\s\xa0]+/g, "");
  }), d = c.length - 5;
  if (0 > d || c.indexOf("Error", d) != d) {
    c += "Error";
  }
  this.name = c;
  c = Error(this.message);
  c.name = this.name;
  this.stack = c.stack || "";
}
z(la, Error);
var ma = "unknown error", A = {15:"element not selectable", 11:"element not visible"};
A[31] = ma;
A[30] = ma;
A[24] = "invalid cookie domain";
A[29] = "invalid element coordinates";
A[12] = "invalid element state";
A[32] = "invalid selector";
A[51] = "invalid selector";
A[52] = "invalid selector";
A[17] = "javascript error";
A[405] = "unsupported operation";
A[34] = "move target out of bounds";
A[27] = "no such alert";
A[7] = "no such element";
A[8] = "no such frame";
A[23] = "no such window";
A[28] = "script timeout";
A[33] = "session not created";
A[10] = "stale element reference";
A[21] = "timeout";
A[25] = "unable to set cookie";
A[26] = "unexpected alert open";
A[13] = ma;
A[9] = "unknown command";
la.prototype.toString = function() {
  return this.name + ": " + this.message;
};
function na(a) {
  var b = a.status;
  if (0 == b) {
    return a;
  }
  b = b || 13;
  a = a.value;
  if (!a || !da(a)) {
    throw new la(b, a + "");
  }
  throw new la(b, a.message + "");
}
;function oa() {
  0 != pa && (qa[x(this)] = this);
  this.ra = this.ra;
  this.Ka = this.Ka;
}
var pa = 0, qa = {};
oa.prototype.ra = !1;
oa.prototype.N = function() {
  if (!this.ra && (this.ra = !0, this.o(), 0 != pa)) {
    var a = x(this);
    delete qa[a];
  }
};
function ra(a, b) {
  a.ra ? b.call(void 0) : (a.Ka || (a.Ka = []), a.Ka.push(n(void 0) ? y(b, void 0) : b));
}
oa.prototype.o = function() {
  if (this.Ka) {
    for (;this.Ka.length;) {
      this.Ka.shift()();
    }
  }
};
function B(a) {
  a && "function" == typeof a.N && a.N();
}
;function sa(a) {
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, sa);
  } else {
    var b = Error().stack;
    b && (this.stack = b);
  }
  a && (this.message = String(a));
}
z(sa, Error);
sa.prototype.name = "CustomError";
var ua;
function va(a, b) {
  for (var c = a.split("%s"), d = "", f = Array.prototype.slice.call(arguments, 1);f.length && 1 < c.length;) {
    d += c.shift() + f.shift();
  }
  return d + c.join("%s");
}
var wa = String.prototype.trim ? function(a) {
  return a.trim();
} : function(a) {
  return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "");
};
function xa(a) {
  if (!ya.test(a)) {
    return a;
  }
  -1 != a.indexOf("&") && (a = a.replace(za, "&amp;"));
  -1 != a.indexOf("<") && (a = a.replace(Aa, "&lt;"));
  -1 != a.indexOf(">") && (a = a.replace(Ba, "&gt;"));
  -1 != a.indexOf('"') && (a = a.replace(Ca, "&quot;"));
  -1 != a.indexOf("'") && (a = a.replace(Da, "&#39;"));
  -1 != a.indexOf("\x00") && (a = a.replace(Ea, "&#0;"));
  return a;
}
var za = /&/g, Aa = /</g, Ba = />/g, Ca = /"/g, Da = /'/g, Ea = /\x00/g, ya = /[\x00&<>"']/;
function Fa(a, b) {
  return Array(b + 1).join(a);
}
function Ga(a, b) {
  return a < b ? -1 : a > b ? 1 : 0;
}
function Ha(a) {
  return String(a).replace(/\-([a-z])/g, function(a, c) {
    return c.toUpperCase();
  });
}
function Ia(a) {
  var b = v(void 0) ? "undefined".replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, "\\$1").replace(/\x08/g, "\\x08") : "\\s";
  return a.replace(new RegExp("(^" + (b ? "|[" + b + "]+" : "") + ")([a-z])", "g"), function(a, b, f) {
    return b + f.toUpperCase();
  });
}
;function Ja(a, b) {
  b.unshift(a);
  sa.call(this, va.apply(null, b));
  b.shift();
}
z(Ja, sa);
Ja.prototype.name = "AssertionError";
function Ka(a, b) {
  throw new Ja("Failure" + (a ? ": " + a : ""), Array.prototype.slice.call(arguments, 1));
}
;function La() {
  var a = Ma;
  return a[a.length - 1];
}
var C = Array.prototype, Oa = C.indexOf ? function(a, b, c) {
  return C.indexOf.call(a, b, c);
} : function(a, b, c) {
  c = null == c ? 0 : 0 > c ? Math.max(0, a.length + c) : c;
  if (v(a)) {
    return v(b) && 1 == b.length ? a.indexOf(b, c) : -1;
  }
  for (;c < a.length;c++) {
    if (c in a && a[c] === b) {
      return c;
    }
  }
  return -1;
}, D = C.forEach ? function(a, b, c) {
  C.forEach.call(a, b, c);
} : function(a, b, c) {
  for (var d = a.length, f = v(a) ? a.split("") : a, g = 0;g < d;g++) {
    g in f && b.call(c, f[g], g, a);
  }
}, Pa = C.filter ? function(a, b, c) {
  return C.filter.call(a, b, c);
} : function(a, b, c) {
  for (var d = a.length, f = [], g = 0, h = v(a) ? a.split("") : a, k = 0;k < d;k++) {
    if (k in h) {
      var l = h[k];
      b.call(c, l, k, a) && (f[g++] = l);
    }
  }
  return f;
}, Qa = C.map ? function(a, b, c) {
  return C.map.call(a, b, c);
} : function(a, b, c) {
  for (var d = a.length, f = Array(d), g = v(a) ? a.split("") : a, h = 0;h < d;h++) {
    h in g && (f[h] = b.call(c, g[h], h, a));
  }
  return f;
}, Ra = C.some ? function(a, b, c) {
  return C.some.call(a, b, c);
} : function(a, b, c) {
  for (var d = a.length, f = v(a) ? a.split("") : a, g = 0;g < d;g++) {
    if (g in f && b.call(c, f[g], g, a)) {
      return !0;
    }
  }
  return !1;
}, Sa = C.every ? function(a, b, c) {
  return C.every.call(a, b, c);
} : function(a, b, c) {
  for (var d = a.length, f = v(a) ? a.split("") : a, g = 0;g < d;g++) {
    if (g in f && !b.call(c, f[g], g, a)) {
      return !1;
    }
  }
  return !0;
};
function Ta(a, b) {
  return 0 <= Oa(a, b);
}
function Ua(a, b) {
  var c = Oa(a, b), d;
  (d = 0 <= c) && C.splice.call(a, c, 1);
  return d;
}
function Va(a) {
  return C.concat.apply(C, arguments);
}
function Wa(a) {
  var b = a.length;
  if (0 < b) {
    for (var c = Array(b), d = 0;d < b;d++) {
      c[d] = a[d];
    }
    return c;
  }
  return [];
}
function Xa(a, b, c, d) {
  C.splice.apply(a, Ya(arguments, 1));
}
function Ya(a, b, c) {
  return 2 >= arguments.length ? C.slice.call(a, b) : C.slice.call(a, b, c);
}
;function Za(a, b) {
  for (var c in a) {
    b.call(void 0, a[c], c, a);
  }
}
function $a(a) {
  var b = [], c = 0, d;
  for (d in a) {
    b[c++] = a[d];
  }
  return b;
}
var ab = "constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" ");
function cb(a, b) {
  for (var c, d, f = 1;f < arguments.length;f++) {
    d = arguments[f];
    for (c in d) {
      a[c] = d[c];
    }
    for (var g = 0;g < ab.length;g++) {
      c = ab[g], Object.prototype.hasOwnProperty.call(d, c) && (a[c] = d[c]);
    }
  }
}
function db(a) {
  var b = arguments.length;
  if (1 == b && t(arguments[0])) {
    return db.apply(null, arguments[0]);
  }
  if (b % 2) {
    throw Error("Uneven number of arguments");
  }
  for (var c = {}, d = 0;d < b;d += 2) {
    c[arguments[d]] = arguments[d + 1];
  }
  return c;
}
function eb(a) {
  var b = arguments.length;
  if (1 == b && t(arguments[0])) {
    return eb.apply(null, arguments[0]);
  }
  for (var c = {}, d = 0;d < b;d++) {
    c[arguments[d]] = !0;
  }
  return c;
}
;function fb(a) {
  if ("function" == typeof a.fa) {
    return a.fa();
  }
  if (v(a)) {
    return a.split("");
  }
  if (ca(a)) {
    for (var b = [], c = a.length, d = 0;d < c;d++) {
      b.push(a[d]);
    }
    return b;
  }
  return $a(a);
}
;function gb(a, b) {
  this.b = {};
  this.a = [];
  this.c = 0;
  var c = arguments.length;
  if (1 < c) {
    if (c % 2) {
      throw Error("Uneven number of arguments");
    }
    for (var d = 0;d < c;d += 2) {
      this.na(arguments[d], arguments[d + 1]);
    }
  } else {
    if (a) {
      if (a instanceof gb) {
        d = a.ab(), c = a.fa();
      } else {
        var c = [], f = 0;
        for (d in a) {
          c[f++] = d;
        }
        d = c;
        c = $a(a);
      }
      for (f = 0;f < d.length;f++) {
        this.na(d[f], c[f]);
      }
    }
  }
}
e = gb.prototype;
e.fa = function() {
  hb(this);
  for (var a = [], b = 0;b < this.a.length;b++) {
    a.push(this.b[this.a[b]]);
  }
  return a;
};
e.ab = function() {
  hb(this);
  return this.a.concat();
};
e.clear = function() {
  this.b = {};
  this.c = this.a.length = 0;
};
e.remove = function(a) {
  return ib(this.b, a) ? (delete this.b[a], this.c--, this.a.length > 2 * this.c && hb(this), !0) : !1;
};
function hb(a) {
  if (a.c != a.a.length) {
    for (var b = 0, c = 0;b < a.a.length;) {
      var d = a.a[b];
      ib(a.b, d) && (a.a[c++] = d);
      b++;
    }
    a.a.length = c;
  }
  if (a.c != a.a.length) {
    for (var f = {}, c = b = 0;b < a.a.length;) {
      d = a.a[b], ib(f, d) || (a.a[c++] = d, f[d] = 1), b++;
    }
    a.a.length = c;
  }
}
function jb(a, b) {
  return ib(a.b, b) ? a.b[b] : void 0;
}
e.na = function(a, b) {
  ib(this.b, a) || (this.c++, this.a.push(a));
  this.b[a] = b;
};
e.forEach = function(a, b) {
  for (var c = this.ab(), d = 0;d < c.length;d++) {
    var f = c[d];
    a.call(b, jb(this, f), f, this);
  }
};
e.clone = function() {
  return new gb(this);
};
function ib(a, b) {
  return Object.prototype.hasOwnProperty.call(a, b);
}
;var E;
a: {
  var kb = m.navigator;
  if (kb) {
    var lb = kb.userAgent;
    if (lb) {
      E = lb;
      break a;
    }
  }
  E = "";
}
;function mb() {
  return -1 != E.indexOf("Edge") || -1 != E.indexOf("Trident") || -1 != E.indexOf("MSIE");
}
;function nb() {
  return -1 != E.indexOf("Edge");
}
;var ob = -1 != E.indexOf("Opera") || -1 != E.indexOf("OPR"), F = mb(), H = -1 != E.indexOf("Gecko") && !(-1 != E.toLowerCase().indexOf("webkit") && !nb()) && !(-1 != E.indexOf("Trident") || -1 != E.indexOf("MSIE")) && !nb(), I = -1 != E.toLowerCase().indexOf("webkit") && !nb(), pb = -1 != E.indexOf("Macintosh");
function qb() {
  var a = E;
  if (H) {
    return /rv\:([^\);]+)(\)|;)/.exec(a);
  }
  if (F && nb()) {
    return /Edge\/([\d\.]+)/.exec(a);
  }
  if (F) {
    return /\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(a);
  }
  if (I) {
    return /WebKit\/(\S+)/.exec(a);
  }
}
function rb() {
  var a = m.document;
  return a ? a.documentMode : void 0;
}
var sb = function() {
  if (ob && m.opera) {
    var a = m.opera.version;
    return w(a) ? a() : a;
  }
  var a = "", b = qb();
  b && (a = b ? b[1] : "");
  return F && !nb() && (b = rb(), b > parseFloat(a)) ? String(b) : a;
}(), tb = {};
function J(a) {
  var b;
  if (!(b = tb[a])) {
    b = 0;
    for (var c = wa(String(sb)).split("."), d = wa(String(a)).split("."), f = Math.max(c.length, d.length), g = 0;0 == b && g < f;g++) {
      var h = c[g] || "", k = d[g] || "", l = RegExp("(\\d*)(\\D*)", "g"), r = RegExp("(\\d*)(\\D*)", "g");
      do {
        var p = l.exec(h) || ["", "", ""], u = r.exec(k) || ["", "", ""];
        if (0 == p[0].length && 0 == u[0].length) {
          break;
        }
        b = Ga(0 == p[1].length ? 0 : parseInt(p[1], 10), 0 == u[1].length ? 0 : parseInt(u[1], 10)) || Ga(0 == p[2].length, 0 == u[2].length) || Ga(p[2], u[2]);
      } while (0 == b);
    }
    b = tb[a] = 0 <= b;
  }
  return b;
}
function ub(a) {
  return F && (nb() || vb >= a);
}
var wb = m.document, xb = rb(), vb = !wb || !F || !xb && nb() ? void 0 : xb || ("CSS1Compat" == wb.compatMode ? parseInt(sb, 10) : 5);
var yb = /^(?:([^:/?#.]+):)?(?:\/\/(?:([^/?#]*)@)?([^/#?]*?)(?::([0-9]+))?(?=[/#?]|$))?([^?#]+)?(?:\?([^#]*))?(?:#(.*))?$/;
function zb(a) {
  if (Ab) {
    Ab = !1;
    var b = m.location;
    if (b) {
      var c = b.href;
      if (c && (c = (c = zb(c)[3] || null) ? decodeURI(c) : c) && c != b.hostname) {
        throw Ab = !0, Error();
      }
    }
  }
  return a.match(yb);
}
var Ab = I;
function Bb(a, b) {
  for (var c = a.split("&"), d = 0;d < c.length;d++) {
    var f = c[d].indexOf("="), g = null, h = null;
    0 <= f ? (g = c[d].substring(0, f), h = c[d].substring(f + 1)) : g = c[d];
    b(g, h ? decodeURIComponent(h.replace(/\+/g, " ")) : "");
  }
}
;function Cb(a, b) {
  this.j = this.B = this.c = "";
  this.w = null;
  this.h = this.l = "";
  this.a = !1;
  var c;
  a instanceof Cb ? (this.a = n(b) ? b : a.a, Db(this, a.c), this.B = a.B, this.j = a.j, Eb(this, a.w), this.l = a.l, Fb(this, a.b.clone()), this.h = a.h) : a && (c = zb(String(a))) ? (this.a = !!b, Db(this, c[1] || "", !0), this.B = Gb(c[2] || ""), this.j = Gb(c[3] || "", !0), Eb(this, c[4]), this.l = Gb(c[5] || "", !0), Fb(this, c[6] || "", !0), this.h = Gb(c[7] || "")) : (this.a = !!b, this.b = new Hb(null, 0, this.a));
}
Cb.prototype.toString = function() {
  var a = [], b = this.c;
  b && a.push(Ib(b, Jb, !0), ":");
  if (b = this.j) {
    a.push("//");
    var c = this.B;
    c && a.push(Ib(c, Jb, !0), "@");
    a.push(encodeURIComponent(String(b)).replace(/%25([0-9a-fA-F]{2})/g, "%$1"));
    b = this.w;
    null != b && a.push(":", String(b));
  }
  if (b = this.l) {
    this.j && "/" != b.charAt(0) && a.push("/"), a.push(Ib(b, "/" == b.charAt(0) ? Kb : Lb, !0));
  }
  (b = this.b.toString()) && a.push("?", b);
  (b = this.h) && a.push("#", Ib(b, Mb));
  return a.join("");
};
Cb.prototype.clone = function() {
  return new Cb(this);
};
function Db(a, b, c) {
  a.c = c ? Gb(b, !0) : b;
  a.c && (a.c = a.c.replace(/:$/, ""));
}
function Eb(a, b) {
  if (b) {
    b = Number(b);
    if (isNaN(b) || 0 > b) {
      throw Error("Bad port number " + b);
    }
    a.w = b;
  } else {
    a.w = null;
  }
}
function Fb(a, b, c) {
  b instanceof Hb ? (a.b = b, Nb(a.b, a.a)) : (c || (b = Ib(b, Ob)), a.b = new Hb(b, 0, a.a));
}
function Gb(a, b) {
  return a ? b ? decodeURI(a.replace(/%25/g, "%2525")) : decodeURIComponent(a) : "";
}
function Ib(a, b, c) {
  return v(a) ? (a = encodeURI(a).replace(b, Pb), c && (a = a.replace(/%25([0-9a-fA-F]{2})/g, "%$1")), a) : null;
}
function Pb(a) {
  a = a.charCodeAt(0);
  return "%" + (a >> 4 & 15).toString(16) + (a & 15).toString(16);
}
var Jb = /[#\/\?@]/g, Lb = /[\#\?:]/g, Kb = /[\#\?]/g, Ob = /[\#\?@]/g, Mb = /#/g;
function Hb(a, b, c) {
  this.c = this.a = null;
  this.b = a || null;
  this.j = !!c;
}
function Qb(a) {
  a.a || (a.a = new gb, a.c = 0, a.b && Bb(a.b, function(b, c) {
    a.add(decodeURIComponent(b.replace(/\+/g, " ")), c);
  }));
}
e = Hb.prototype;
e.add = function(a, b) {
  Qb(this);
  this.b = null;
  a = Rb(this, a);
  var c = jb(this.a, a);
  c || this.a.na(a, c = []);
  c.push(b);
  this.c++;
  return this;
};
e.remove = function(a) {
  Qb(this);
  a = Rb(this, a);
  return ib(this.a.b, a) ? (this.b = null, this.c -= jb(this.a, a).length, this.a.remove(a)) : !1;
};
e.clear = function() {
  this.a = this.b = null;
  this.c = 0;
};
e.ab = function() {
  Qb(this);
  for (var a = this.a.fa(), b = this.a.ab(), c = [], d = 0;d < b.length;d++) {
    for (var f = a[d], g = 0;g < f.length;g++) {
      c.push(b[d]);
    }
  }
  return c;
};
e.fa = function(a) {
  Qb(this);
  var b = [];
  if (v(a)) {
    var c = a;
    Qb(this);
    c = Rb(this, c);
    ib(this.a.b, c) && (b = Va(b, jb(this.a, Rb(this, a))));
  } else {
    for (a = this.a.fa(), c = 0;c < a.length;c++) {
      b = Va(b, a[c]);
    }
  }
  return b;
};
e.toString = function() {
  if (this.b) {
    return this.b;
  }
  if (!this.a) {
    return "";
  }
  for (var a = [], b = this.a.ab(), c = 0;c < b.length;c++) {
    for (var d = b[c], f = encodeURIComponent(String(d)), d = this.fa(d), g = 0;g < d.length;g++) {
      var h = f;
      "" !== d[g] && (h += "=" + encodeURIComponent(String(d[g])));
      a.push(h);
    }
  }
  return this.b = a.join("&");
};
e.clone = function() {
  var a = new Hb;
  a.b = this.b;
  this.a && (a.a = this.a.clone(), a.c = this.c);
  return a;
};
function Rb(a, b) {
  var c = String(b);
  a.j && (c = c.toLowerCase());
  return c;
}
function Nb(a, b) {
  b && !a.j && (Qb(a), a.b = null, a.a.forEach(function(a, b) {
    var f = b.toLowerCase();
    b != f && (this.remove(b), this.remove(f), 0 < a.length && (this.b = null, this.a.na(Rb(this, f), Wa(a)), this.c += a.length));
  }, a));
  a.j = b;
}
;eb("area base br col command embed hr img input keygen link meta param source track wbr".split(" "));
function Sb() {
  this.a = "";
}
Sb.prototype.b = function() {
  return this.a;
};
Sb.prototype.toString = function() {
  return "SafeStyle{" + this.a + "}";
};
function Tb(a) {
  var b = new Sb;
  b.a = a;
  return b;
}
Tb("");
function Ub() {
  this.a = "";
  this.c = Vb;
  this.j = null;
}
Ub.prototype.b = function() {
  return this.a;
};
Ub.prototype.toString = function() {
  return "SafeHtml{" + this.a + "}";
};
function Wb(a) {
  if (a instanceof Ub && a.constructor === Ub && a.c === Vb) {
    return a.a;
  }
  Ka("expected object of type SafeHtml, got '" + a + "'");
  return "type_error:SafeHtml";
}
var Vb = {};
function Xb(a, b) {
  var c = new Ub;
  c.a = a;
  c.j = b;
  return c;
}
Xb("<!DOCTYPE html>", 0);
var Yb = Xb("", 0);
function Zb(a) {
  this.a = new gb;
  if (a) {
    a = fb(a);
    for (var b = a.length, c = 0;c < b;c++) {
      this.add(a[c]);
    }
  }
}
function $b(a) {
  var b = typeof a;
  return "object" == b && a || "function" == b ? "o" + x(a) : b.substr(0, 1) + a;
}
e = Zb.prototype;
e.add = function(a) {
  this.a.na($b(a), a);
};
e.remove = function(a) {
  return this.a.remove($b(a));
};
e.clear = function() {
  this.a.clear();
};
e.contains = function(a) {
  a = $b(a);
  return ib(this.a.b, a);
};
e.fa = function() {
  return this.a.fa();
};
e.clone = function() {
  return new Zb(this);
};
function ac(a, b, c, d, f) {
  this.reset(a, b, c, d, f);
}
ac.prototype.a = null;
var bc = 0;
ac.prototype.reset = function(a, b, c, d, f) {
  "number" == typeof f || bc++;
  this.j = d || ja();
  this.h = a;
  this.c = b;
  this.b = c;
  delete this.a;
};
function cc(a) {
  this.j = a;
  this.a = this.C = this.c = this.b = null;
}
function dc(a, b) {
  this.name = a;
  this.value = b;
}
dc.prototype.toString = function() {
  return this.name;
};
var ec = new dc("SHOUT", 1200), fc = new dc("SEVERE", 1E3), gc = new dc("WARNING", 900), hc = new dc("INFO", 800), ic = new dc("CONFIG", 700), jc = new dc("FINER", 400);
function kc(a) {
  if (a.c) {
    return a.c;
  }
  if (a.b) {
    return kc(a.b);
  }
  Ka("Root logger has no level set.");
  return null;
}
cc.prototype.log = function(a, b, c) {
  if (a.value >= kc(this).value) {
    for (w(b) && (b = b()), a = new ac(a, String(b), this.j), c && (a.a = c), c = "log:" + a.c, m.console && (m.console.timeStamp ? m.console.timeStamp(c) : m.console.markTimeline && m.console.markTimeline(c)), m.msWriteProfilerMark && m.msWriteProfilerMark(c), c = this;c;) {
      b = c;
      var d = a;
      if (b.a) {
        for (var f = 0, g = void 0;g = b.a[f];f++) {
          g(d);
        }
      }
      c = c.b;
    }
  }
};
function lc(a, b) {
  a.log(jc, b, void 0);
}
var mc = {}, nc = null;
function oc() {
  nc || (nc = new cc(""), mc[""] = nc, nc.c = ic);
}
function pc(a) {
  oc();
  var b;
  if (!(b = mc[a])) {
    b = new cc(a);
    var c = a.lastIndexOf("."), d = a.substr(c + 1), c = pc(a.substr(0, c));
    c.C || (c.C = {});
    c.C[d] = b;
    b.b = c;
    mc[a] = b;
  }
  return b;
}
;function qc() {
  this.a = ja();
}
var rc = new qc;
qc.prototype.reset = function() {
  this.a = ja();
};
function sc(a) {
  this.j = a || "";
  this.h = rc;
}
sc.prototype.a = !0;
sc.prototype.b = !0;
sc.prototype.c = !1;
function tc(a) {
  return 10 > a ? "0" + a : String(a);
}
function uc(a) {
  sc.call(this, a);
}
z(uc, sc);
function vc() {
  this.j = y(this.h, this);
  this.a = new uc;
  this.a.b = !1;
  this.a.c = !1;
  this.b = this.a.a = !1;
  this.c = "";
  this.l = {};
}
function wc(a, b) {
  if (b != a.b) {
    var c;
    oc();
    c = nc;
    if (b) {
      var d = a.j;
      c.a || (c.a = []);
      c.a.push(d);
    } else {
      (c = c.a) && Ua(c, a.j);
    }
    a.b = b;
  }
}
vc.prototype.h = function(a) {
  if (!this.l[a.b]) {
    var b;
    b = this.a;
    var c = [];
    c.push(b.j, " ");
    if (b.b) {
      var d = new Date(a.j);
      c.push("[", tc(d.getFullYear() - 2E3) + tc(d.getMonth() + 1) + tc(d.getDate()) + " " + tc(d.getHours()) + ":" + tc(d.getMinutes()) + ":" + tc(d.getSeconds()) + "." + tc(Math.floor(d.getMilliseconds() / 10)), "] ");
    }
    var d = (a.j - b.h.a) / 1E3, f = d.toFixed(3), g = 0;
    if (1 > d) {
      g = 2;
    } else {
      for (;100 > d;) {
        g++, d *= 10;
      }
    }
    for (;0 < g--;) {
      f = " " + f;
    }
    c.push("[", f, "s] ");
    c.push("[", a.b, "] ");
    c.push(a.c);
    b.c && (d = a.a) && c.push("\n", d instanceof Error ? d.message : d.toString());
    b.a && c.push("\n");
    b = c.join("");
    if (c = xc) {
      switch(a.h) {
        case ec:
          yc(c, "info", b);
          break;
        case fc:
          yc(c, "error", b);
          break;
        case gc:
          yc(c, "warn", b);
          break;
        default:
          yc(c, "debug", b);
      }
    } else {
      this.c += b;
    }
  }
};
var xc = m.console;
function yc(a, b, c) {
  if (a[b]) {
    a[b](c);
  } else {
    a.log(c);
  }
}
;var zc = !F || ub(9), Ac = !F || ub(9), Bc = F && !J("9");
!I || J("528");
H && J("1.9b") || F && J("8") || ob && J("9.5") || I && J("528");
H && !J("8") || F && J("9");
function K(a, b) {
  this.type = a;
  this.c = this.target = b;
  this.l = !1;
  this.Uc = !0;
}
K.prototype.w = function() {
  this.l = !0;
};
K.prototype.j = function() {
  this.Uc = !1;
};
function Cc(a) {
  Cc[" "](a);
  return a;
}
Cc[" "] = q;
function L(a, b) {
  K.call(this, a ? a.type : "");
  this.h = this.c = this.target = null;
  this.a = this.clientY = this.clientX = 0;
  this.metaKey = this.shiftKey = this.altKey = this.ctrlKey = !1;
  this.state = null;
  this.B = !1;
  this.b = null;
  a && Dc(this, a, b);
}
z(L, K);
var Ec = [1, 4, 2];
function Dc(a, b, c) {
  var d = a.type = b.type;
  a.target = b.target || b.srcElement;
  a.c = c;
  if (c = b.relatedTarget) {
    if (H) {
      var f;
      a: {
        try {
          Cc(c.nodeName);
          f = !0;
          break a;
        } catch (g) {
        }
        f = !1;
      }
      f || (c = null);
    }
  } else {
    "mouseover" == d ? c = b.fromElement : "mouseout" == d && (c = b.toElement);
  }
  a.h = c;
  a.clientX = void 0 !== b.clientX ? b.clientX : b.pageX;
  a.clientY = void 0 !== b.clientY ? b.clientY : b.pageY;
  a.a = b.keyCode || 0;
  a.ctrlKey = b.ctrlKey;
  a.altKey = b.altKey;
  a.shiftKey = b.shiftKey;
  a.metaKey = b.metaKey;
  a.B = pb ? b.metaKey : b.ctrlKey;
  a.state = b.state;
  a.b = b;
  b.defaultPrevented && a.j();
}
function Fc(a) {
  return (zc ? 0 == a.b.button : "click" == a.type ? !0 : !!(a.b.button & Ec[0])) && !(I && pb && a.ctrlKey);
}
L.prototype.w = function() {
  L.m.w.call(this);
  this.b.stopPropagation ? this.b.stopPropagation() : this.b.cancelBubble = !0;
};
L.prototype.j = function() {
  L.m.j.call(this);
  var a = this.b;
  if (a.preventDefault) {
    a.preventDefault();
  } else {
    if (a.returnValue = !1, Bc) {
      try {
        if (a.ctrlKey || 112 <= a.keyCode && 123 >= a.keyCode) {
          a.keyCode = -1;
        }
      } catch (b) {
      }
    }
  }
};
var Gc = "closure_listenable_" + (1E6 * Math.random() | 0);
function Hc(a) {
  return !(!a || !a[Gc]);
}
var Ic = 0;
function Jc(a, b, c, d, f) {
  this.listener = a;
  this.a = null;
  this.src = b;
  this.type = c;
  this.qb = !!d;
  this.Ab = f;
  this.key = ++Ic;
  this.Wa = this.pb = !1;
}
function Kc(a) {
  a.Wa = !0;
  a.listener = null;
  a.a = null;
  a.src = null;
  a.Ab = null;
}
;function Lc(a) {
  this.src = a;
  this.a = {};
  this.b = 0;
}
Lc.prototype.add = function(a, b, c, d, f) {
  var g = a.toString();
  a = this.a[g];
  a || (a = this.a[g] = [], this.b++);
  var h = Mc(a, b, d, f);
  -1 < h ? (b = a[h], c || (b.pb = !1)) : (b = new Jc(b, this.src, g, !!d, f), b.pb = c, a.push(b));
  return b;
};
Lc.prototype.remove = function(a, b, c, d) {
  a = a.toString();
  if (!(a in this.a)) {
    return !1;
  }
  var f = this.a[a];
  b = Mc(f, b, c, d);
  return -1 < b ? (Kc(f[b]), C.splice.call(f, b, 1), 0 == f.length && (delete this.a[a], this.b--), !0) : !1;
};
function Nc(a, b) {
  var c = b.type;
  if (!(c in a.a)) {
    return !1;
  }
  var d = Ua(a.a[c], b);
  d && (Kc(b), 0 == a.a[c].length && (delete a.a[c], a.b--));
  return d;
}
function Oc(a, b, c, d, f) {
  a = a.a[b.toString()];
  b = -1;
  a && (b = Mc(a, c, d, f));
  return -1 < b ? a[b] : null;
}
function Mc(a, b, c, d) {
  for (var f = 0;f < a.length;++f) {
    var g = a[f];
    if (!g.Wa && g.listener == b && g.qb == !!c && g.Ab == d) {
      return f;
    }
  }
  return -1;
}
;var Pc = "closure_lm_" + (1E6 * Math.random() | 0), Qc = {}, Rc = 0;
function M(a, b, c, d, f) {
  if (t(b)) {
    for (var g = 0;g < b.length;g++) {
      M(a, b[g], c, d, f);
    }
    return null;
  }
  c = Sc(c);
  return Hc(a) ? a.v(b, c, d, f) : Tc(a, b, c, !1, d, f);
}
function Tc(a, b, c, d, f, g) {
  if (!b) {
    throw Error("Invalid event type");
  }
  var h = !!f, k = Uc(a);
  k || (a[Pc] = k = new Lc(a));
  c = k.add(b, c, d, f, g);
  if (c.a) {
    return c;
  }
  d = Vc();
  c.a = d;
  d.src = a;
  d.listener = c;
  if (a.addEventListener) {
    a.addEventListener(b.toString(), d, h);
  } else {
    if (a.attachEvent) {
      a.attachEvent(Wc(b.toString()), d);
    } else {
      throw Error("addEventListener and attachEvent are unavailable.");
    }
  }
  Rc++;
  return c;
}
function Vc() {
  var a = Xc, b = Ac ? function(c) {
    return a.call(b.src, b.listener, c);
  } : function(c) {
    c = a.call(b.src, b.listener, c);
    if (!c) {
      return c;
    }
  };
  return b;
}
function Yc(a, b, c, d, f) {
  if (t(b)) {
    for (var g = 0;g < b.length;g++) {
      Yc(a, b[g], c, d, f);
    }
  } else {
    c = Sc(c), Hc(a) ? a.ma.add(String(b), c, !0, d, f) : Tc(a, b, c, !0, d, f);
  }
}
function Zc(a, b, c, d, f) {
  if (t(b)) {
    for (var g = 0;g < b.length;g++) {
      Zc(a, b[g], c, d, f);
    }
  } else {
    c = Sc(c), Hc(a) ? a.ia(b, c, d, f) : a && (a = Uc(a)) && (b = Oc(a, b, c, !!d, f)) && $c(b);
  }
}
function $c(a) {
  if ("number" == typeof a || !a || a.Wa) {
    return !1;
  }
  var b = a.src;
  if (Hc(b)) {
    return Nc(b.ma, a);
  }
  var c = a.type, d = a.a;
  b.removeEventListener ? b.removeEventListener(c, d, a.qb) : b.detachEvent && b.detachEvent(Wc(c), d);
  Rc--;
  (c = Uc(b)) ? (Nc(c, a), 0 == c.b && (c.src = null, b[Pc] = null)) : Kc(a);
  return !0;
}
function ad(a) {
  if (a) {
    if (Hc(a)) {
      a.Ib(void 0);
    } else {
      if (a = Uc(a)) {
        var b = 0, c;
        for (c in a.a) {
          for (var d = a.a[c].concat(), f = 0;f < d.length;++f) {
            $c(d[f]) && ++b;
          }
        }
      }
    }
  }
}
function Wc(a) {
  return a in Qc ? Qc[a] : Qc[a] = "on" + a;
}
function bd(a, b, c, d) {
  var f = !0;
  if (a = Uc(a)) {
    if (b = a.a[b.toString()]) {
      for (b = b.concat(), a = 0;a < b.length;a++) {
        var g = b[a];
        g && g.qb == c && !g.Wa && (g = cd(g, d), f = f && !1 !== g);
      }
    }
  }
  return f;
}
function cd(a, b) {
  var c = a.listener, d = a.Ab || a.src;
  a.pb && $c(a);
  return c.call(d, b);
}
function Xc(a, b) {
  if (a.Wa) {
    return !0;
  }
  if (!Ac) {
    var c;
    if (!(c = b)) {
      a: {
        c = ["window", "event"];
        for (var d = m, f;f = c.shift();) {
          if (null != d[f]) {
            d = d[f];
          } else {
            c = null;
            break a;
          }
        }
        c = d;
      }
    }
    f = c;
    c = new L(f, this);
    d = !0;
    if (!(0 > f.keyCode || void 0 != f.returnValue)) {
      a: {
        var g = !1;
        if (0 == f.keyCode) {
          try {
            f.keyCode = -1;
            break a;
          } catch (h) {
            g = !0;
          }
        }
        if (g || void 0 == f.returnValue) {
          f.returnValue = !0;
        }
      }
      f = [];
      for (g = c.c;g;g = g.parentNode) {
        f.push(g);
      }
      for (var g = a.type, k = f.length - 1;!c.l && 0 <= k;k--) {
        c.c = f[k];
        var l = bd(f[k], g, !0, c), d = d && l;
      }
      for (k = 0;!c.l && k < f.length;k++) {
        c.c = f[k], l = bd(f[k], g, !1, c), d = d && l;
      }
    }
    return d;
  }
  return cd(a, new L(b, this));
}
function Uc(a) {
  a = a[Pc];
  return a instanceof Lc ? a : null;
}
var dd = "__closure_events_fn_" + (1E9 * Math.random() >>> 0);
function Sc(a) {
  if (w(a)) {
    return a;
  }
  a[dd] || (a[dd] = function(b) {
    return a.handleEvent(b);
  });
  return a[dd];
}
;function ed(a, b) {
  a && a.log(gc, b, void 0);
}
function fd(a, b) {
  a && a.log(hc, b, void 0);
}
;var gd = !F || ub(9), hd = !H && !F || F && ub(9) || H && J("1.9.1"), id = F && !J("9");
function N(a, b) {
  this.x = n(a) ? a : 0;
  this.y = n(b) ? b : 0;
}
e = N.prototype;
e.clone = function() {
  return new N(this.x, this.y);
};
e.toString = function() {
  return "(" + this.x + ", " + this.y + ")";
};
function jd(a, b) {
  return new N(a.x - b.x, a.y - b.y);
}
e.ceil = function() {
  this.x = Math.ceil(this.x);
  this.y = Math.ceil(this.y);
  return this;
};
e.floor = function() {
  this.x = Math.floor(this.x);
  this.y = Math.floor(this.y);
  return this;
};
e.round = function() {
  this.x = Math.round(this.x);
  this.y = Math.round(this.y);
  return this;
};
function kd(a, b) {
  this.width = a;
  this.height = b;
}
e = kd.prototype;
e.clone = function() {
  return new kd(this.width, this.height);
};
e.toString = function() {
  return "(" + this.width + " x " + this.height + ")";
};
e.ceil = function() {
  this.width = Math.ceil(this.width);
  this.height = Math.ceil(this.height);
  return this;
};
e.floor = function() {
  this.width = Math.floor(this.width);
  this.height = Math.floor(this.height);
  return this;
};
e.round = function() {
  this.width = Math.round(this.width);
  this.height = Math.round(this.height);
  return this;
};
function O(a) {
  return a ? new ld(P(a)) : ua || (ua = new ld);
}
function md(a, b) {
  Za(b, function(b, d) {
    "style" == d ? a.style.cssText = b : "class" == d ? a.className = b : "for" == d ? a.htmlFor = b : d in nd ? a.setAttribute(nd[d], b) : 0 == d.lastIndexOf("aria-", 0) || 0 == d.lastIndexOf("data-", 0) ? a.setAttribute(d, b) : a[d] = b;
  });
}
var nd = {cellpadding:"cellPadding", cellspacing:"cellSpacing", colspan:"colSpan", frameborder:"frameBorder", height:"height", maxlength:"maxLength", role:"role", rowspan:"rowSpan", type:"type", usemap:"useMap", valign:"vAlign", width:"width"};
function od(a) {
  a = a.document;
  a = "CSS1Compat" == a.compatMode ? a.documentElement : a.body;
  return new kd(a.clientWidth, a.clientHeight);
}
function pd(a) {
  return a.a ? a.a : I || "CSS1Compat" != a.compatMode ? a.body || a.documentElement : a.documentElement;
}
function qd(a) {
  return a ? rd(a) : window;
}
function rd(a) {
  return a.parentWindow || a.defaultView;
}
function sd(a, b, c) {
  return td(document, arguments);
}
function td(a, b) {
  var c = b[0], d = b[1];
  if (!gd && d && (d.name || d.type)) {
    c = ["<", c];
    d.name && c.push(' name="', xa(d.name), '"');
    if (d.type) {
      c.push(' type="', xa(d.type), '"');
      var f = {};
      cb(f, d);
      delete f.type;
      d = f;
    }
    c.push(">");
    c = c.join("");
  }
  c = a.createElement(c);
  d && (v(d) ? c.className = d : t(d) ? c.className = d.join(" ") : md(c, d));
  2 < b.length && ud(a, c, b, 2);
  return c;
}
function ud(a, b, c, d) {
  function f(c) {
    c && b.appendChild(v(c) ? a.createTextNode(c) : c);
  }
  for (;d < c.length;d++) {
    var g = c[d];
    !ca(g) || da(g) && 0 < g.nodeType ? f(g) : D(vd(g) ? Wa(g) : g, f);
  }
}
function wd(a, b) {
  ud(P(a), a, arguments, 1);
}
function xd(a) {
  for (var b;b = a.firstChild;) {
    a.removeChild(b);
  }
}
function yd(a) {
  return a && a.parentNode ? a.parentNode.removeChild(a) : null;
}
function zd(a, b) {
  if (a.contains && 1 == b.nodeType) {
    return a == b || a.contains(b);
  }
  if ("undefined" != typeof a.compareDocumentPosition) {
    return a == b || Boolean(a.compareDocumentPosition(b) & 16);
  }
  for (;b && a != b;) {
    b = b.parentNode;
  }
  return b == a;
}
function P(a) {
  return 9 == a.nodeType ? a : a.ownerDocument || a.document;
}
function Ad(a, b) {
  if ("textContent" in a) {
    a.textContent = b;
  } else {
    if (3 == a.nodeType) {
      a.data = b;
    } else {
      if (a.firstChild && 3 == a.firstChild.nodeType) {
        for (;a.lastChild != a.firstChild;) {
          a.removeChild(a.lastChild);
        }
        a.firstChild.data = b;
      } else {
        xd(a), a.appendChild(P(a).createTextNode(String(b)));
      }
    }
  }
}
var Bd = {SCRIPT:1, STYLE:1, HEAD:1, IFRAME:1, OBJECT:1}, Cd = {IMG:" ", BR:"\n"};
function Dd(a, b) {
  b ? a.tabIndex = 0 : (a.tabIndex = -1, a.removeAttribute("tabIndex"));
}
function Ed(a) {
  a = a.getAttributeNode("tabindex");
  return null != a && a.specified;
}
function Fd(a) {
  a = a.tabIndex;
  return "number" == typeof a && 0 <= a && 32768 > a;
}
function Gd(a) {
  var b = [];
  Hd(a, b, !1);
  return b.join("");
}
function Hd(a, b, c) {
  if (!(a.nodeName in Bd)) {
    if (3 == a.nodeType) {
      c ? b.push(String(a.nodeValue).replace(/(\r\n|\r|\n)/g, "")) : b.push(a.nodeValue);
    } else {
      if (a.nodeName in Cd) {
        b.push(Cd[a.nodeName]);
      } else {
        for (a = a.firstChild;a;) {
          Hd(a, b, c), a = a.nextSibling;
        }
      }
    }
  }
}
function vd(a) {
  if (a && "number" == typeof a.length) {
    if (da(a)) {
      return "function" == typeof a.item || "string" == typeof a.item;
    }
    if (w(a)) {
      return "function" == typeof a.item;
    }
  }
  return !1;
}
function ld(a) {
  this.a = a || m.document || document;
}
e = ld.prototype;
e.g = function(a) {
  return v(a) ? this.a.getElementById(a) : a;
};
e.D = function(a, b, c) {
  return td(this.a, arguments);
};
function Id(a) {
  return "CSS1Compat" == a.a.compatMode;
}
function Jd(a) {
  var b = a.a;
  a = pd(b);
  b = rd(b);
  return F && J("10") && b.pageYOffset != a.scrollTop ? new N(a.scrollLeft, a.scrollTop) : new N(b.pageXOffset || a.scrollLeft, b.pageYOffset || a.scrollTop);
}
e.vc = yd;
function Kd(a) {
  return hd && void 0 != a.children ? a.children : Pa(a.childNodes, function(a) {
    return 1 == a.nodeType;
  });
}
e.contains = zd;
e.Vd = Ad;
function Q(a, b, c, d) {
  this.top = a;
  this.right = b;
  this.bottom = c;
  this.left = d;
}
e = Q.prototype;
e.clone = function() {
  return new Q(this.top, this.right, this.bottom, this.left);
};
e.toString = function() {
  return "(" + this.top + "t, " + this.right + "r, " + this.bottom + "b, " + this.left + "l)";
};
e.contains = function(a) {
  return this && a ? a instanceof Q ? a.left >= this.left && a.right <= this.right && a.top >= this.top && a.bottom <= this.bottom : a.x >= this.left && a.x <= this.right && a.y >= this.top && a.y <= this.bottom : !1;
};
function Ld(a, b) {
  var c = b.x < a.left ? b.x - a.left : b.x > a.right ? b.x - a.right : 0, d = b.y < a.top ? b.y - a.top : b.y > a.bottom ? b.y - a.bottom : 0;
  return Math.sqrt(c * c + d * d);
}
e.ceil = function() {
  this.top = Math.ceil(this.top);
  this.right = Math.ceil(this.right);
  this.bottom = Math.ceil(this.bottom);
  this.left = Math.ceil(this.left);
  return this;
};
e.floor = function() {
  this.top = Math.floor(this.top);
  this.right = Math.floor(this.right);
  this.bottom = Math.floor(this.bottom);
  this.left = Math.floor(this.left);
  return this;
};
e.round = function() {
  this.top = Math.round(this.top);
  this.right = Math.round(this.right);
  this.bottom = Math.round(this.bottom);
  this.left = Math.round(this.left);
  return this;
};
function Md(a, b, c, d) {
  this.left = a;
  this.top = b;
  this.width = c;
  this.height = d;
}
e = Md.prototype;
e.clone = function() {
  return new Md(this.left, this.top, this.width, this.height);
};
function Nd(a) {
  return new Q(a.top, a.left + a.width, a.top + a.height, a.left);
}
e.toString = function() {
  return "(" + this.left + ", " + this.top + " - " + this.width + "w x " + this.height + "h)";
};
e.contains = function(a) {
  return a instanceof Md ? this.left <= a.left && this.left + this.width >= a.left + a.width && this.top <= a.top && this.top + this.height >= a.top + a.height : a.x >= this.left && a.x <= this.left + this.width && a.y >= this.top && a.y <= this.top + this.height;
};
e.ceil = function() {
  this.left = Math.ceil(this.left);
  this.top = Math.ceil(this.top);
  this.width = Math.ceil(this.width);
  this.height = Math.ceil(this.height);
  return this;
};
e.floor = function() {
  this.left = Math.floor(this.left);
  this.top = Math.floor(this.top);
  this.width = Math.floor(this.width);
  this.height = Math.floor(this.height);
  return this;
};
e.round = function() {
  this.left = Math.round(this.left);
  this.top = Math.round(this.top);
  this.width = Math.round(this.width);
  this.height = Math.round(this.height);
  return this;
};
function Od(a, b, c) {
  if (v(b)) {
    (b = Pd(a, b)) && (a.style[b] = c);
  } else {
    for (var d in b) {
      c = a;
      var f = b[d], g = Pd(c, d);
      g && (c.style[g] = f);
    }
  }
}
var Qd = {};
function Pd(a, b) {
  var c = Qd[b];
  if (!c) {
    var d = Ha(b), c = d;
    void 0 === a.style[d] && (d = (I ? "Webkit" : H ? "Moz" : F ? "ms" : ob ? "O" : null) + Ia(d), void 0 !== a.style[d] && (c = d));
    Qd[b] = c;
  }
  return c;
}
function Rd(a, b) {
  var c = P(a);
  return c.defaultView && c.defaultView.getComputedStyle && (c = c.defaultView.getComputedStyle(a, null)) ? c[b] || c.getPropertyValue(b) || "" : "";
}
function Sd(a, b) {
  return Rd(a, b) || (a.currentStyle ? a.currentStyle[b] : null) || a.style && a.style[b];
}
function Td(a, b, c) {
  var d;
  b instanceof N ? (d = b.x, b = b.y) : (d = b, b = c);
  a.style.left = Ud(d, !1);
  a.style.top = Ud(b, !1);
}
function Vd(a) {
  a = a ? P(a) : document;
  return !F || ub(9) || Id(O(a)) ? a.documentElement : a.body;
}
function Wd(a) {
  var b;
  try {
    b = a.getBoundingClientRect();
  } catch (c) {
    return {left:0, top:0, right:0, bottom:0};
  }
  F && a.ownerDocument.body && (a = a.ownerDocument, b.left -= a.documentElement.clientLeft + a.body.clientLeft, b.top -= a.documentElement.clientTop + a.body.clientTop);
  return b;
}
function Xd(a) {
  if (F && !ub(8)) {
    return a.offsetParent;
  }
  var b = P(a), c = Sd(a, "position"), d = "fixed" == c || "absolute" == c;
  for (a = a.parentNode;a && a != b;a = a.parentNode) {
    if (11 == a.nodeType && a.host && (a = a.host), c = Sd(a, "position"), d = d && "static" == c && a != b.documentElement && a != b.body, !d && (a.scrollWidth > a.clientWidth || a.scrollHeight > a.clientHeight || "fixed" == c || "absolute" == c || "relative" == c)) {
      return a;
    }
  }
  return null;
}
function Yd(a) {
  for (var b = new Q(0, Infinity, Infinity, 0), c = O(a), d = c.a.body, f = c.a.documentElement, g = pd(c.a);a = Xd(a);) {
    if (!(F && 0 == a.clientWidth || I && 0 == a.clientHeight && a == d) && a != d && a != f && "visible" != Sd(a, "overflow")) {
      var h = Zd(a), k = new N(a.clientLeft, a.clientTop);
      h.x += k.x;
      h.y += k.y;
      b.top = Math.max(b.top, h.y);
      b.right = Math.min(b.right, h.x + a.clientWidth);
      b.bottom = Math.min(b.bottom, h.y + a.clientHeight);
      b.left = Math.max(b.left, h.x);
    }
  }
  d = g.scrollLeft;
  g = g.scrollTop;
  b.left = Math.max(b.left, d);
  b.top = Math.max(b.top, g);
  c = od(rd(c.a) || window);
  b.right = Math.min(b.right, d + c.width);
  b.bottom = Math.min(b.bottom, g + c.height);
  return 0 <= b.top && 0 <= b.left && b.bottom > b.top && b.right > b.left ? b : null;
}
function Zd(a) {
  var b = P(a), c = new N(0, 0), d = Vd(b);
  if (a == d) {
    return c;
  }
  a = Wd(a);
  b = Jd(O(b));
  c.x = a.left + b.x;
  c.y = a.top + b.y;
  return c;
}
function $d(a, b, c) {
  if (b instanceof kd) {
    c = b.height, b = b.width;
  } else {
    if (void 0 == c) {
      throw Error("missing height argument");
    }
  }
  a.style.width = Ud(b, !0);
  a.style.height = Ud(c, !0);
}
function Ud(a, b) {
  "number" == typeof a && (a = (b ? Math.round(a) : a) + "px");
  return a;
}
function ae(a) {
  var b = be;
  if ("none" != Sd(a, "display")) {
    return b(a);
  }
  var c = a.style, d = c.display, f = c.visibility, g = c.position;
  c.visibility = "hidden";
  c.position = "absolute";
  c.display = "inline";
  a = b(a);
  c.display = d;
  c.position = g;
  c.visibility = f;
  return a;
}
function be(a) {
  var b = a.offsetWidth, c = a.offsetHeight, d = I && !b && !c;
  return n(b) && !d || !a.getBoundingClientRect ? new kd(b, c) : (a = Wd(a), new kd(a.right - a.left, a.bottom - a.top));
}
function ce(a) {
  var b = Zd(a);
  a = ae(a);
  return new Md(b.x, b.y, a.width, a.height);
}
function de(a, b) {
  var c = a.style;
  "opacity" in c ? c.opacity = b : "MozOpacity" in c ? c.MozOpacity = b : "filter" in c && (c.filter = "" === b ? "" : "alpha(opacity=" + 100 * b + ")");
}
function R(a, b) {
  a.style.display = b ? "" : "none";
}
function ee(a) {
  return "rtl" == Sd(a, "direction");
}
var fe = H ? "MozUserSelect" : I ? "WebkitUserSelect" : null;
function ge(a, b, c) {
  c = c ? null : a.getElementsByTagName("*");
  if (fe) {
    if (b = b ? "none" : "", a.style && (a.style[fe] = b), c) {
      a = 0;
      for (var d;d = c[a];a++) {
        d.style && (d.style[fe] = b);
      }
    }
  } else {
    if (F || ob) {
      if (b = b ? "on" : "", a.setAttribute("unselectable", b), c) {
        for (a = 0;d = c[a];a++) {
          d.setAttribute("unselectable", b);
        }
      }
    }
  }
}
function he(a, b) {
  if (/^\d+px?$/.test(b)) {
    return parseInt(b, 10);
  }
  var c = a.style.left, d = a.runtimeStyle.left;
  a.runtimeStyle.left = a.currentStyle.left;
  a.style.left = b;
  var f = a.style.pixelLeft;
  a.style.left = c;
  a.runtimeStyle.left = d;
  return f;
}
function ie(a, b) {
  var c = a.currentStyle ? a.currentStyle[b] : null;
  return c ? he(a, c) : 0;
}
var je = {thin:2, medium:4, thick:6};
function ke(a, b) {
  if ("none" == (a.currentStyle ? a.currentStyle[b + "Style"] : null)) {
    return 0;
  }
  var c = a.currentStyle ? a.currentStyle[b + "Width"] : null;
  return c in je ? je[c] : he(a, c);
}
F && J(12);
function le(a) {
  oa.call(this);
  this.b = a;
  this.a = {};
}
z(le, oa);
var me = [];
le.prototype.v = function(a, b, c, d) {
  t(b) || (b && (me[0] = b.toString()), b = me);
  for (var f = 0;f < b.length;f++) {
    var g = M(a, b[f], c || this.handleEvent, d || !1, this.b || this);
    if (!g) {
      break;
    }
    this.a[g.key] = g;
  }
  return this;
};
le.prototype.ia = function(a, b, c, d, f) {
  if (t(b)) {
    for (var g = 0;g < b.length;g++) {
      this.ia(a, b[g], c, d, f);
    }
  } else {
    c = c || this.handleEvent, f = f || this.b || this, c = Sc(c), d = !!d, b = Hc(a) ? Oc(a.ma, String(b), c, d, f) : a ? (a = Uc(a)) ? Oc(a, b, c, d, f) : null : null, b && ($c(b), delete this.a[b.key]);
  }
  return this;
};
function ne(a) {
  Za(a.a, $c);
  a.a = {};
}
le.prototype.o = function() {
  le.m.o.call(this);
  ne(this);
};
le.prototype.handleEvent = function() {
  throw Error("EventHandler.handleEvent not implemented");
};
function S() {
  oa.call(this);
  this.ma = new Lc(this);
  this.bd = this;
  this.nb = null;
}
z(S, oa);
S.prototype[Gc] = !0;
e = S.prototype;
e.jc = function(a) {
  this.nb = a;
};
e.addEventListener = function(a, b, c, d) {
  M(this, a, b, c, d);
};
e.removeEventListener = function(a, b, c, d) {
  Zc(this, a, b, c, d);
};
e.F = function(a) {
  var b, c = this.nb;
  if (c) {
    for (b = [];c;c = c.nb) {
      b.push(c);
    }
  }
  var c = this.bd, d = a.type || a;
  if (v(a)) {
    a = new K(a, c);
  } else {
    if (a instanceof K) {
      a.target = a.target || c;
    } else {
      var f = a;
      a = new K(d, c);
      cb(a, f);
    }
  }
  var f = !0, g;
  if (b) {
    for (var h = b.length - 1;!a.l && 0 <= h;h--) {
      g = a.c = b[h], f = oe(g, d, !0, a) && f;
    }
  }
  a.l || (g = a.c = c, f = oe(g, d, !0, a) && f, a.l || (f = oe(g, d, !1, a) && f));
  if (b) {
    for (h = 0;!a.l && h < b.length;h++) {
      g = a.c = b[h], f = oe(g, d, !1, a) && f;
    }
  }
  return f;
};
e.o = function() {
  S.m.o.call(this);
  this.Ib();
  this.nb = null;
};
e.v = function(a, b, c, d) {
  return this.ma.add(String(a), b, !1, c, d);
};
e.ia = function(a, b, c, d) {
  return this.ma.remove(String(a), b, c, d);
};
e.Ib = function(a) {
  var b;
  if (this.ma) {
    b = this.ma;
    a = a && a.toString();
    var c = 0, d;
    for (d in b.a) {
      if (!a || d == a) {
        for (var f = b.a[d], g = 0;g < f.length;g++) {
          ++c, Kc(f[g]);
        }
        delete b.a[d];
        b.b--;
      }
    }
    b = c;
  } else {
    b = 0;
  }
  return b;
};
function oe(a, b, c, d) {
  b = a.ma.a[String(b)];
  if (!b) {
    return !0;
  }
  b = b.concat();
  for (var f = !0, g = 0;g < b.length;++g) {
    var h = b[g];
    if (h && !h.Wa && h.qb == c) {
      var k = h.listener, l = h.Ab || h.src;
      h.pb && Nc(a.ma, h);
      f = !1 !== k.call(l, d) && f;
    }
  }
  return f && 0 != d.Uc;
}
;function pe() {
}
aa(pe);
pe.prototype.a = 0;
function T(a) {
  S.call(this);
  this.a = a || O();
  this.sa = qe;
  this.ka = null;
  this.L = !1;
  this.b = null;
  this.B = void 0;
  this.w = this.C = this.j = null;
}
z(T, S);
T.prototype.Za = pe.Ea();
var qe = null;
function re(a, b) {
  switch(a) {
    case 1:
      return b ? "disable" : "enable";
    case 2:
      return b ? "highlight" : "unhighlight";
    case 4:
      return b ? "activate" : "deactivate";
    case 8:
      return b ? "select" : "unselect";
    case 16:
      return b ? "check" : "uncheck";
    case 32:
      return b ? "focus" : "blur";
    case 64:
      return b ? "open" : "close";
  }
  throw Error("Invalid component state");
}
e = T.prototype;
e.O = function() {
  return this.ka || (this.ka = ":" + (this.Za.a++).toString(36));
};
e.g = function() {
  return this.b;
};
function U(a) {
  a.B || (a.B = new le(a));
  return a.B;
}
function se(a, b) {
  if (a == b) {
    throw Error("Unable to set parent component");
  }
  if (b && a.j && a.ka && te(a.j, a.ka) && a.j != b) {
    throw Error("Unable to set parent component");
  }
  a.j = b;
  T.m.jc.call(a, b);
}
e.jc = function(a) {
  if (this.j && this.j != a) {
    throw Error("Method not supported");
  }
  T.m.jc.call(this, a);
};
e.K = function() {
  this.b = this.a.a.createElement("DIV");
};
function ue(a, b, c) {
  if (a.L) {
    throw Error("Component already rendered");
  }
  a.b || a.K();
  b ? b.insertBefore(a.b, c || null) : a.a.a.body.appendChild(a.b);
  a.j && !a.j.L || a.T();
}
e.T = function() {
  this.L = !0;
  ve(this, function(a) {
    !a.L && a.g() && a.T();
  });
};
e.ba = function() {
  ve(this, function(a) {
    a.L && a.ba();
  });
  this.B && ne(this.B);
  this.L = !1;
};
e.o = function() {
  this.L && this.ba();
  this.B && (this.B.N(), delete this.B);
  ve(this, function(a) {
    a.N();
  });
  this.b && yd(this.b);
  this.j = this.b = this.w = this.C = null;
  T.m.o.call(this);
};
e.ga = function(a, b) {
  this.ob(a, we(this), b);
};
e.ob = function(a, b, c) {
  if (a.L && (c || !this.L)) {
    throw Error("Component already rendered");
  }
  if (0 > b || b > we(this)) {
    throw Error("Child component index out of bounds");
  }
  this.w && this.C || (this.w = {}, this.C = []);
  if (a.j == this) {
    var d = a.O();
    this.w[d] = a;
    Ua(this.C, a);
  } else {
    var d = this.w, f = a.O();
    if (f in d) {
      throw Error('The object already contains the key "' + f + '"');
    }
    d[f] = a;
  }
  se(a, this);
  Xa(this.C, b, 0, a);
  a.L && this.L && a.j == this ? (c = this.Fa(), b = c.childNodes[b] || null, b != a.g() && c.insertBefore(a.g(), b)) : c ? (this.b || this.K(), b = V(this, b + 1), ue(a, this.Fa(), b ? b.b : null)) : this.L && !a.L && a.b && a.b.parentNode && 1 == a.b.parentNode.nodeType && a.T();
};
e.Fa = function() {
  return this.b;
};
function ye(a) {
  null == a.sa && (a.sa = ee(a.L ? a.b : a.a.a.body));
  return a.sa;
}
function we(a) {
  return a.C ? a.C.length : 0;
}
function te(a, b) {
  var c;
  a.w && b ? (c = a.w, c = (b in c ? c[b] : void 0) || null) : c = null;
  return c;
}
function V(a, b) {
  return a.C ? a.C[b] || null : null;
}
function ve(a, b, c) {
  a.C && D(a.C, b, c);
}
function ze(a, b) {
  return a.C && b ? Oa(a.C, b) : -1;
}
e.removeChild = function(a, b) {
  if (a) {
    var c = v(a) ? a : a.O();
    a = te(this, c);
    if (c && a) {
      var d = this.w;
      c in d && delete d[c];
      Ua(this.C, a);
      b && (a.ba(), a.b && yd(a.b));
      se(a, null);
    }
  }
  if (!a) {
    throw Error("Child is not in parent component");
  }
  return a;
};
function Ae() {
  T.call(this);
}
z(Ae, T);
e = Ae.prototype;
e.fc = null;
e.o = function() {
  ad(this.g());
  $c(this.fc);
  this.fc = null;
  Ae.m.o.call(this);
};
e.K = function() {
  var a = this.a.D("DIV", "banner");
  Od(a, "position", "absolute");
  Od(a, "top", "0");
  M(a, "click", y(this.Hb, this, !1));
  this.b = a;
  this.Jb();
  this.fc = M(qd(this.a.a) || window, "resize", this.Jb, !1, this);
};
e.Hb = function(a) {
  R(this.g(), a);
  this.Jb();
};
e.Jb = function() {
  if (!this.g().style.display) {
    var a = qd(this.a.a) || window, b = Jd(this.a).x, c = ae(this.g()), a = Math.max(b + od(a || window).width / 2 - c.width / 2, 0);
    Td(this.g(), a, 0);
  }
};
function Be(a, b, c) {
  K.call(this, a, b);
  this.data = c;
}
z(Be, K);
var Ce;
function De(a, b) {
  b ? a.setAttribute("role", b) : a.removeAttribute("role");
}
function Ee(a, b, c) {
  t(c) && (c = c.join(" "));
  var d = "aria-" + b;
  "" === c || void 0 == c ? (Ce || (Ce = {atomic:!1, autocomplete:"none", dropeffect:"none", haspopup:!1, live:"off", multiline:!1, multiselectable:!1, orientation:"vertical", readonly:!1, relevant:"additions text", required:!1, sort:"none", busy:!1, disabled:!1, hidden:!1, invalid:"false"}), c = Ce, b in c ? a.setAttribute(d, c[b]) : a.removeAttribute(d)) : a.setAttribute(d, c);
}
;function Fe(a) {
  if (a.classList) {
    return a.classList;
  }
  a = a.className;
  return v(a) && a.match(/\S+/g) || [];
}
function Ge(a, b) {
  return a.classList ? a.classList.contains(b) : Ta(Fe(a), b);
}
function He(a, b) {
  a.classList ? a.classList.add(b) : Ge(a, b) || (a.className += 0 < a.className.length ? " " + b : b);
}
function Ie(a, b) {
  if (a.classList) {
    D(b, function(b) {
      He(a, b);
    });
  } else {
    var c = {};
    D(Fe(a), function(a) {
      c[a] = !0;
    });
    D(b, function(a) {
      c[a] = !0;
    });
    a.className = "";
    for (var d in c) {
      a.className += 0 < a.className.length ? " " + d : d;
    }
  }
}
function Je(a, b) {
  a.classList ? a.classList.remove(b) : Ge(a, b) && (a.className = Pa(Fe(a), function(a) {
    return a != b;
  }).join(" "));
}
function Ke(a, b) {
  a.classList ? D(b, function(b) {
    Je(a, b);
  }) : a.className = Pa(Fe(a), function(a) {
    return !Ta(b, a);
  }).join(" ");
}
;function Le(a, b, c, d, f) {
  if (!(F || I && J("525"))) {
    return !0;
  }
  if (pb && f) {
    return Me(a);
  }
  if (f && !d) {
    return !1;
  }
  "number" == typeof b && (b = Ne(b));
  if (!c && (17 == b || 18 == b || pb && 91 == b)) {
    return !1;
  }
  if (I && d && c) {
    switch(a) {
      case 220:
      ;
      case 219:
      ;
      case 221:
      ;
      case 192:
      ;
      case 186:
      ;
      case 189:
      ;
      case 187:
      ;
      case 188:
      ;
      case 190:
      ;
      case 191:
      ;
      case 192:
      ;
      case 222:
        return !1;
    }
  }
  if (F && d && b == a) {
    return !1;
  }
  switch(a) {
    case 13:
      return !0;
    case 27:
      return !I;
  }
  return Me(a);
}
function Me(a) {
  if (48 <= a && 57 >= a || 96 <= a && 106 >= a || 65 <= a && 90 >= a || I && 0 == a) {
    return !0;
  }
  switch(a) {
    case 32:
    ;
    case 63:
    ;
    case 107:
    ;
    case 109:
    ;
    case 110:
    ;
    case 111:
    ;
    case 186:
    ;
    case 59:
    ;
    case 189:
    ;
    case 187:
    ;
    case 61:
    ;
    case 188:
    ;
    case 190:
    ;
    case 191:
    ;
    case 192:
    ;
    case 222:
    ;
    case 219:
    ;
    case 220:
    ;
    case 221:
      return !0;
    default:
      return !1;
  }
}
function Ne(a) {
  if (H) {
    a = Oe(a);
  } else {
    if (pb && I) {
      a: {
        switch(a) {
          case 93:
            a = 91;
            break a;
        }
      }
    }
  }
  return a;
}
function Oe(a) {
  switch(a) {
    case 61:
      return 187;
    case 59:
      return 186;
    case 173:
      return 189;
    case 224:
      return 91;
    case 0:
      return 224;
    default:
      return a;
  }
}
;function Pe(a, b, c) {
  S.call(this);
  this.target = a;
  this.j = b || a;
  this.c = c || new Md(NaN, NaN, NaN, NaN);
  this.b = P(a);
  this.a = new le(this);
  ra(this, ia(B, this.a));
  M(this.j, ["touchstart", "mousedown"], this.Wc, !1, this);
}
z(Pe, S);
var Qe = F && !J("12") || H && J("1.9.3");
e = Pe.prototype;
e.clientX = 0;
e.clientY = 0;
e.Xc = 0;
e.Yc = 0;
e.Qa = 0;
e.Ra = 0;
e.Ub = !0;
e.Da = !1;
e.ea = function(a) {
  this.Ub = a;
};
e.o = function() {
  Pe.m.o.call(this);
  Zc(this.j, ["touchstart", "mousedown"], this.Wc, !1, this);
  ne(this.a);
  Qe && this.b.releaseCapture();
  this.j = this.target = null;
};
e.Wc = function(a) {
  var b = "mousedown" == a.type;
  if (!this.Ub || this.Da || b && !Fc(a)) {
    this.F("earlycancel");
  } else {
    if (Re(a), this.F(new Se("start", this, a.clientX, a.clientY))) {
      this.Da = !0;
      a.j();
      var b = this.b, c = b.documentElement, d = !Qe;
      this.a.v(b, ["touchmove", "mousemove"], this.ud, d);
      this.a.v(b, ["touchend", "mouseup"], this.rb, d);
      Qe ? (c.setCapture(!1), this.a.v(c, "losecapture", this.rb)) : this.a.v(qd(b), "blur", this.rb);
      this.l && this.a.v(this.l, "scroll", this.Ld, d);
      this.clientX = this.Xc = a.clientX;
      this.clientY = this.Yc = a.clientY;
      this.Qa = this.target.offsetLeft;
      this.Ra = this.target.offsetTop;
      this.h = Jd(O(this.b));
      ja();
    }
  }
};
e.rb = function(a) {
  ne(this.a);
  Qe && this.b.releaseCapture();
  if (this.Da) {
    Re(a);
    this.Da = !1;
    var b = Te(this, this.Qa), c = Ue(this, this.Ra);
    this.F(new Se("end", this, a.clientX, a.clientY, 0, b, c));
  } else {
    this.F("earlycancel");
  }
};
function Re(a) {
  var b = a.type;
  "touchstart" == b || "touchmove" == b ? Dc(a, a.b.targetTouches[0], a.c) : "touchend" != b && "touchcancel" != b || Dc(a, a.b.changedTouches[0], a.c);
}
e.ud = function(a) {
  if (this.Ub) {
    Re(a);
    var b = 1 * (a.clientX - this.clientX), c = a.clientY - this.clientY;
    this.clientX = a.clientX;
    this.clientY = a.clientY;
    if (!this.Da) {
      var d = this.Xc - this.clientX, f = this.Yc - this.clientY;
      if (0 < d * d + f * f) {
        if (this.F(new Se("start", this, a.clientX, a.clientY))) {
          this.Da = !0;
        } else {
          this.ra || this.rb(a);
          return;
        }
      }
    }
    c = Ve(this, b, c);
    b = c.x;
    c = c.y;
    this.Da && this.F(new Se("beforedrag", this, a.clientX, a.clientY, 0, b, c)) && (We(this, a, b, c), a.j());
  }
};
function Ve(a, b, c) {
  var d = Jd(O(a.b));
  b += d.x - a.h.x;
  c += d.y - a.h.y;
  a.h = d;
  a.Qa += b;
  a.Ra += c;
  b = Te(a, a.Qa);
  a = Ue(a, a.Ra);
  return new N(b, a);
}
e.Ld = function(a) {
  var b = Ve(this, 0, 0);
  a.clientX = this.clientX;
  a.clientY = this.clientY;
  We(this, a, b.x, b.y);
};
function We(a, b, c, d) {
  a.target.style.left = c + "px";
  a.target.style.top = d + "px";
  a.F(new Se("drag", a, b.clientX, b.clientY, 0, c, d));
}
function Te(a, b) {
  var c = a.c, d = isNaN(c.left) ? null : c.left, c = isNaN(c.width) ? 0 : c.width;
  return Math.min(null != d ? d + c : Infinity, Math.max(null != d ? d : -Infinity, b));
}
function Ue(a, b) {
  var c = a.c, d = isNaN(c.top) ? null : c.top, c = isNaN(c.height) ? 0 : c.height;
  return Math.min(null != d ? d + c : Infinity, Math.max(null != d ? d : -Infinity, b));
}
function Se(a, b, c, d, f, g, h) {
  K.call(this, a);
  this.clientX = c;
  this.clientY = d;
  this.left = n(g) ? g : b.Qa;
  this.top = n(h) ? h : b.Ra;
}
z(Se, K);
function Xe(a, b, c) {
  this.j = c;
  this.c = a;
  this.h = b;
  this.b = 0;
  this.a = null;
}
Xe.prototype.put = function(a) {
  this.h(a);
  this.b < this.j && (this.b++, a.next = this.a, this.a = a);
};
function Ye() {
  this.b = this.a = null;
}
var $e = new Xe(function() {
  return new Ze;
}, function(a) {
  a.reset();
}, 100);
Ye.prototype.add = function(a, b) {
  var c;
  0 < $e.b ? ($e.b--, c = $e.a, $e.a = c.next, c.next = null) : c = $e.c();
  c.Na = a;
  c.scope = b;
  c.next = null;
  this.b ? this.b.next = c : this.a = c;
  this.b = c;
};
Ye.prototype.remove = function() {
  var a = null;
  this.a && (a = this.a, this.a = this.a.next, this.a || (this.b = null), a.next = null);
  return a;
};
function Ze() {
  this.next = this.scope = this.Na = null;
}
Ze.prototype.reset = function() {
  this.next = this.scope = this.Na = null;
};
function af(a) {
  m.setTimeout(function() {
    throw a;
  }, 0);
}
var bf;
function cf() {
  var a = m.MessageChannel;
  "undefined" === typeof a && "undefined" !== typeof window && window.postMessage && window.addEventListener && -1 == E.indexOf("Presto") && (a = function() {
    var a = document.createElement("IFRAME");
    a.style.display = "none";
    a.src = "";
    document.documentElement.appendChild(a);
    var b = a.contentWindow, a = b.document;
    a.open();
    a.write("");
    a.close();
    var c = "callImmediate" + Math.random(), d = "file:" == b.location.protocol ? "*" : b.location.protocol + "//" + b.location.host, a = y(function(a) {
      if (("*" == d || a.origin == d) && a.data == c) {
        this.port1.onmessage();
      }
    }, this);
    b.addEventListener("message", a, !1);
    this.port1 = {};
    this.port2 = {postMessage:function() {
      b.postMessage(c, d);
    }};
  });
  if ("undefined" !== typeof a && !mb()) {
    var b = new a, c = {}, d = c;
    b.port1.onmessage = function() {
      if (n(c.next)) {
        c = c.next;
        var a = c.oc;
        c.oc = null;
        a();
      }
    };
    return function(a) {
      d.next = {oc:a};
      d = d.next;
      b.port2.postMessage(0);
    };
  }
  return "undefined" !== typeof document && "onreadystatechange" in document.createElement("SCRIPT") ? function(a) {
    var b = document.createElement("SCRIPT");
    b.onreadystatechange = function() {
      b.onreadystatechange = null;
      b.parentNode.removeChild(b);
      b = null;
      a();
      a = null;
    };
    document.documentElement.appendChild(b);
  } : function(a) {
    m.setTimeout(a, 0);
  };
}
;function df(a, b) {
  ef || ff();
  gf || (ef(), gf = !0);
  hf.add(a, b);
}
var ef;
function ff() {
  if (m.Promise && m.Promise.resolve) {
    var a = m.Promise.resolve();
    ef = function() {
      a.then(jf);
    };
  } else {
    ef = function() {
      var a = jf;
      !w(m.setImmediate) || m.Window && m.Window.prototype && m.Window.prototype.setImmediate == m.setImmediate ? (bf || (bf = cf()), bf(a)) : m.setImmediate(a);
    };
  }
}
var gf = !1, hf = new Ye;
function jf() {
  for (var a = null;a = hf.remove();) {
    try {
      a.Na.call(a.scope);
    } catch (b) {
      af(b);
    }
    $e.put(a);
  }
  gf = !1;
}
;function kf(a, b, c) {
  if (w(a)) {
    c && (a = y(a, c));
  } else {
    if (a && "function" == typeof a.handleEvent) {
      a = y(a.handleEvent, a);
    } else {
      throw Error("Invalid listener argument");
    }
  }
  return 2147483647 < b ? -1 : m.setTimeout(a, b || 0);
}
;var lf = F ? 'javascript:""' : "about:blank";
function mf(a) {
  S.call(this);
  this.a = a;
  a = F ? "focusout" : "blur";
  this.b = M(this.a, F ? "focusin" : "focus", this, !F);
  this.c = M(this.a, a, this, !F);
}
z(mf, S);
mf.prototype.handleEvent = function(a) {
  var b = new L(a.b);
  b.type = "focusin" == a.type || "focus" == a.type ? "focusin" : "focusout";
  this.F(b);
};
mf.prototype.o = function() {
  mf.m.o.call(this);
  $c(this.b);
  $c(this.c);
  delete this.a;
};
function nf(a, b) {
  this.c = a;
  this.b = b;
}
;function of(a, b) {
  S.call(this);
  this.c = new le(this);
  this.hc(a || null);
  b && (this.Ya = b);
}
z(of, S);
e = of.prototype;
e.ca = null;
e.nc = null;
e.Z = !1;
e.dc = -1;
e.Ya = "toggle_display";
e.g = function() {
  return this.ca;
};
e.hc = function(a) {
  if (this.Z) {
    throw Error("Can not change this state of the popup while showing.");
  }
  this.ca = a;
};
e.xa = function(a) {
  this.I && this.I.stop();
  this.G && this.G.stop();
  if (a) {
    if (!this.Z && this.ec()) {
      if (!this.ca) {
        throw Error("Caller must call setElement before trying to show the popup");
      }
      this.fb();
      a = P(this.ca);
      this.c.v(a, "mousedown", this.Pc, !0);
      if (F) {
        var b;
        try {
          b = a.activeElement;
        } catch (c) {
        }
        for (;b && "IFRAME" == b.nodeName;) {
          try {
            var d = b.contentDocument || b.contentWindow.document;
          } catch (f) {
            break;
          }
          a = d;
          b = a.activeElement;
        }
        this.c.v(a, "mousedown", this.Pc, !0);
        this.c.v(a, "deactivate", this.Oc);
      } else {
        this.c.v(a, "blur", this.Oc);
      }
      "toggle_display" == this.Ya ? (this.ca.style.visibility = "visible", R(this.ca, !0)) : "move_offscreen" == this.Ya && this.fb();
      this.Z = !0;
      this.dc = ja();
      this.I ? (Yc(this.I, "end", this.xb, !1, this), this.I.play()) : this.xb();
    }
  } else {
    pf(this);
  }
};
e.fb = q;
function pf(a, b) {
  a.Z && a.F({type:"beforehide", target:b}) && (a.c && ne(a.c), a.Z = !1, ja(), a.G ? (Yc(a.G, "end", ia(a.pc, b), !1, a), a.G.play()) : a.pc(b));
}
e.pc = function(a) {
  "toggle_display" == this.Ya ? this.Cd() : "move_offscreen" == this.Ya && (this.ca.style.top = "-10000px");
  this.ib(a);
};
e.Cd = function() {
  this.ca.style.visibility = "hidden";
  R(this.ca, !1);
};
e.ec = function() {
  return this.F("beforeshow");
};
e.xb = function() {
  this.F("show");
};
e.ib = function(a) {
  this.F({type:"hide", target:a});
};
e.Pc = function(a) {
  a = a.target;
  zd(this.ca, a) || qf(this, a) || 150 > ja() - this.dc || pf(this, a);
};
e.Oc = function(a) {
  var b = P(this.ca);
  if ("undefined" != typeof document.activeElement) {
    if (a = b.activeElement, !a || zd(this.ca, a) || "BODY" == a.tagName) {
      return;
    }
  } else {
    if (a.target != b) {
      return;
    }
  }
  150 > ja() - this.dc || pf(this);
};
function qf(a, b) {
  return Ra(a.nc || [], function(a) {
    return b === a || zd(a, b);
  });
}
e.o = function() {
  of.m.o.call(this);
  this.c.N();
  B(this.I);
  B(this.G);
  delete this.ca;
  delete this.c;
  delete this.nc;
};
function rf(a, b) {
  T.call(this, b);
  this.La = !!a;
  this.A = null;
}
z(rf, T);
e = rf.prototype;
e.Rb = null;
e.wa = !1;
e.aa = null;
e.V = null;
e.ha = null;
e.Ob = !1;
e.W = function() {
  return "goog-modalpopup";
};
e.sb = function() {
  return this.aa;
};
e.K = function() {
  rf.m.K.call(this);
  var a = this.g(), b = wa(this.W()).split(" ");
  Ie(a, b);
  Dd(a, !0);
  R(a, !1);
  this.La && !this.V && (a = this.a, b = v(void 0) ? Tb(void 0).b() : "", this.V = a.D("iframe", {frameborder:0, style:"border:0;vertical-align:bottom;" + b, src:lf}), this.V.className = this.W() + "-bg", R(this.V, !1), de(this.V, 0));
  this.aa || (this.aa = this.a.D("DIV", this.W() + "-bg"), R(this.aa, !1));
  this.ha || (this.ha = this.a.a.createElement("SPAN"), R(this.ha, !1), Dd(this.ha, !0), this.ha.style.position = "absolute");
};
e.Tc = function() {
  this.Ob = !1;
};
e.T = function() {
  if (this.V) {
    var a = this.g();
    a.parentNode && a.parentNode.insertBefore(this.V, a);
  }
  a = this.g();
  a.parentNode && a.parentNode.insertBefore(this.aa, a);
  rf.m.T.call(this);
  a = this.g();
  a.parentNode && a.parentNode.insertBefore(this.ha, a.nextSibling);
  this.Rb = new mf(this.a.a);
  U(this).v(this.Rb, "focusin", this.Jd);
  sf(this, !1);
};
e.ba = function() {
  this.wa && this.X(!1);
  B(this.Rb);
  rf.m.ba.call(this);
  yd(this.V);
  yd(this.aa);
  yd(this.ha);
};
e.X = function(a) {
  if (a != this.wa) {
    if (this.H && this.H.stop(), this.S && this.S.stop(), this.G && this.G.stop(), this.I && this.I.stop(), this.L && sf(this, a), a) {
      if (this.F("beforeshow")) {
        try {
          this.A = this.a.a.activeElement;
        } catch (b) {
        }
        this.gc();
        tf(this);
        U(this).v(rd(this.a.a), "resize", this.gc);
        uf(this, !0);
        this.uc();
        this.wa = !0;
        this.H && this.S ? (Yc(this.H, "end", this.Gb, !1, this), this.S.play(), this.H.play()) : this.Gb();
      }
    } else {
      if (this.F("beforehide")) {
        U(this).ia(rd(this.a.a), "resize", this.gc);
        this.wa = !1;
        this.G && this.I ? (Yc(this.G, "end", this.Fb, !1, this), this.I.play(), this.G.play()) : this.Fb();
        a: {
          try {
            var c = this.a, d = c.a.body, f = c.a.activeElement || d;
            if (!this.A || this.A == d) {
              this.A = null;
              break a;
            }
            (f == d || c.contains(this.g(), f)) && this.A.focus();
          } catch (g) {
          }
          this.A = null;
        }
      }
    }
  }
};
function sf(a, b) {
  a.$ || (a.$ = new nf(a.b, a.a));
  var c = a.$;
  if (b) {
    c.a || (c.a = []);
    for (var d = Kd(c.b.a.body), f = 0;f < d.length;f++) {
      var g = d[f], h;
      if (h = g != c.c) {
        h = g.getAttribute("aria-hidden"), h = !(null == h || void 0 == h ? 0 : String(h));
      }
      h && (Ee(g, "hidden", !0), c.a.push(g));
    }
  } else {
    if (c.a) {
      for (f = 0;f < c.a.length;f++) {
        c.a[f].removeAttribute("aria-hidden");
      }
      c.a = null;
    }
  }
}
function uf(a, b) {
  a.V && R(a.V, b);
  a.aa && R(a.aa, b);
  R(a.g(), b);
  R(a.ha, b);
}
e.Gb = function() {
  this.F("show");
};
e.Fb = function() {
  uf(this, !1);
  this.F("hide");
};
e.gc = function() {
  this.V && R(this.V, !1);
  this.aa && R(this.aa, !1);
  var a = this.a.a, b = od(qd(a) || window || window), c = Math.max(b.width, Math.max(a.body.scrollWidth, a.documentElement.scrollWidth)), a = Math.max(b.height, Math.max(a.body.scrollHeight, a.documentElement.scrollHeight));
  this.V && (R(this.V, !0), $d(this.V, c, a));
  this.aa && (R(this.aa, !0), $d(this.aa, c, a));
};
function tf(a) {
  var b = qd(a.a.a) || window;
  if ("fixed" == Sd(a.g(), "position")) {
    var c = 0, d = 0
  } else {
    d = Jd(a.a), c = d.x, d = d.y;
  }
  var f = ae(a.g()), b = od(b || window), c = Math.max(c + b.width / 2 - f.width / 2, 0), d = Math.max(d + b.height / 2 - f.height / 2, 0);
  Td(a.g(), c, d);
  Td(a.ha, c, d);
}
e.Jd = function(a) {
  this.Ob ? this.Tc() : a.target == this.ha && kf(this.uc, 0, this);
};
e.uc = function() {
  try {
    F && this.a.a.body.focus(), this.g().focus();
  } catch (a) {
  }
};
e.o = function() {
  B(this.H);
  this.H = null;
  B(this.G);
  this.G = null;
  B(this.S);
  this.S = null;
  B(this.I);
  this.I = null;
  rf.m.o.call(this);
};
function W(a, b, c) {
  rf.call(this, b, c);
  this.l = a || "modal-dialog";
  this.c = X(X(new vf, wf, !0), xf, !1, !0);
}
z(W, rf);
e = W.prototype;
e.Nc = !0;
e.Nb = .5;
e.$c = "";
e.Yb = null;
e.ta = null;
e.Xa = null;
e.Ja = null;
e.Zc = null;
e.pa = null;
e.$a = null;
e.ja = null;
e.W = function() {
  return this.l;
};
function yf(a, b) {
  a.$c = b;
  a.Ja && Ad(a.Ja, b);
}
function zf(a, b) {
  a.Yb = b;
  a.$a && (a.$a.innerHTML = Wb(b));
}
e.Fa = function() {
  this.g() || ue(this, void 0);
  return this.$a;
};
e.sb = function() {
  this.g() || ue(this, void 0);
  return W.m.sb.call(this);
};
function Af(a, b) {
  var c = wa(a.l + "-title-draggable").split(" ");
  a.g() && (b ? Ie(a.Xa, c) : Ke(a.Xa, c));
  b && !a.ta ? (a.ta = new Pe(a.g(), a.Xa), Ie(a.Xa, c), M(a.ta, "start", a.Ud, !1, a)) : !b && a.ta && (a.ta.N(), a.ta = null);
}
e.K = function() {
  W.m.K.call(this);
  var a = this.g(), b = this.a;
  this.Xa = b.D("DIV", this.l + "-title", this.Ja = b.D("SPAN", {className:this.l + "-title-text", id:this.O()}, this.$c), this.pa = b.D("SPAN", this.l + "-title-close"));
  wd(a, this.Xa, this.$a = b.D("DIV", this.l + "-content"), this.ja = b.D("DIV", this.l + "-buttons"));
  De(this.Ja, "heading");
  De(this.pa, "button");
  Dd(this.pa, !0);
  Ee(this.pa, "label", Bf);
  this.Zc = this.Ja.id;
  De(a, "dialog");
  Ee(a, "labelledby", this.Zc || "");
  this.Yb && (this.$a.innerHTML = Wb(this.Yb));
  R(this.pa, !0);
  this.c && (a = this.c, a.va = this.ja, Cf(a));
  R(this.ja, !!this.c);
  this.Nb = this.Nb;
  this.g() && (a = this.sb()) && de(a, this.Nb);
};
e.T = function() {
  W.m.T.call(this);
  U(this).v(this.g(), "keydown", this.Qc).v(this.g(), "keypress", this.Qc);
  U(this).v(this.ja, "click", this.Fd);
  Af(this, !0);
  U(this).v(this.pa, "click", this.Nd);
  var a = this.g();
  De(a, "dialog");
  "" !== this.Ja.id && Ee(a, "labelledby", this.Ja.id);
  if (!this.Nc) {
    this.Nc = !1;
    if (this.L) {
      var a = this.a, b = this.sb();
      a.vc(this.V);
      a.vc(b);
    }
    this.wa && sf(this, !1);
  }
};
e.ba = function() {
  this.wa && this.X(!1);
  Af(this, !1);
  W.m.ba.call(this);
};
e.X = function(a) {
  a != this.wa && (this.L || ue(this, void 0), W.m.X.call(this, a));
};
e.Gb = function() {
  W.m.Gb.call(this);
  this.F(Df);
};
e.Fb = function() {
  W.m.Fb.call(this);
  this.F(Ef);
};
e.Ud = function() {
  var a = this.a.a, b = od(qd(a) || window || window), c = Math.max(a.body.scrollWidth, b.width), a = Math.max(a.body.scrollHeight, b.height), d = ae(this.g());
  "fixed" == Sd(this.g(), "position") ? (b = new Md(0, 0, Math.max(0, b.width - d.width), Math.max(0, b.height - d.height)), this.ta.c = b || new Md(NaN, NaN, NaN, NaN)) : this.ta.c = new Md(0, 0, c - d.width, a - d.height) || new Md(NaN, NaN, NaN, NaN);
};
e.Nd = function() {
  Ff(this);
};
function Ff(a) {
  var b = a.c, c = b && b.Pb;
  c ? a.F(new Gf(c, jb(b, c))) && a.X(!1) : a.X(!1);
}
e.o = function() {
  this.ja = this.pa = null;
  W.m.o.call(this);
};
e.Fd = function(a) {
  a: {
    for (a = a.target;null != a && a != this.ja;) {
      if ("BUTTON" == a.tagName) {
        break a;
      }
      a = a.parentNode;
    }
    a = null;
  }
  a && !a.disabled && (a = a.name, this.F(new Gf(a, jb(this.c, a))) && this.X(!1));
};
e.Qc = function(a) {
  var b = !1, c = !1, d = this.c, f = a.target;
  if ("keydown" == a.type) {
    if (27 == a.a) {
      var g = d && d.Pb, f = "SELECT" == f.tagName && !f.disabled;
      g && !f ? (c = !0, b = this.F(new Gf(g, jb(d, g)))) : f || (b = !0);
    } else {
      if (9 == a.a && a.shiftKey && f == this.g()) {
        this.Ob = !0;
        try {
          this.ha.focus();
        } catch (h) {
        }
        kf(this.Tc, 0, this);
      }
    }
  } else {
    if (13 == a.a) {
      if ("BUTTON" == f.tagName && !f.disabled) {
        g = f.name;
      } else {
        if (f == this.pa) {
          Ff(this);
        } else {
          if (d) {
            var k = d.Qb, l;
            if (l = k) {
              a: {
                l = d.va.getElementsByTagName("BUTTON");
                for (var r = 0, p;p = l[r];r++) {
                  if (p.name == k || p.id == k) {
                    l = p;
                    break a;
                  }
                }
                l = null;
              }
            }
            f = ("TEXTAREA" == f.tagName || "SELECT" == f.tagName || "A" == f.tagName) && !f.disabled;
            !l || l.disabled || f || (g = k);
          }
        }
      }
      g && d && (c = !0, b = this.F(new Gf(g, String(jb(d, g)))));
    } else {
      f == this.pa && 32 == a.a && Ff(this);
    }
  }
  if (b || c) {
    a.w(), a.j();
  }
  b && this.X(!1);
};
function Gf(a, b) {
  this.type = Hf;
  this.key = a;
  this.caption = b;
}
z(Gf, K);
var Hf = "dialogselect", Ef = "afterhide", Df = "aftershow";
function vf(a) {
  a || O();
  gb.call(this);
}
z(vf, gb);
e = vf.prototype;
e.Qb = null;
e.va = null;
e.Pb = null;
e.na = function(a, b, c, d) {
  gb.prototype.na.call(this, a, b);
  c && (this.Qb = a);
  d && (this.Pb = a);
  return this;
};
function X(a, b, c, d) {
  return a.na(b.key, b.caption, c, d);
}
function Cf(a) {
  if (a.va) {
    a.va.innerHTML = Wb(Yb);
    var b = O(a.va);
    a.forEach(function(a, d) {
      var f = b.D("BUTTON", {name:d}, a);
      d == this.Qb && (f.className = "goog-buttonset-default");
      this.va.appendChild(f);
    }, a);
  }
}
e.g = function() {
  return this.va;
};
var Bf = "Close", wf = {key:"ok", caption:"OK"}, xf = {key:"cancel", caption:"Cancel"}, If = {key:"yes", caption:"Yes"}, Jf = {key:"no", caption:"No"}, Kf = {key:"save", caption:"Save"}, Lf = {key:"continue", caption:"Continue"};
"undefined" != typeof document && (X(new vf, wf, !0, !0), X(X(new vf, wf, !0), xf, !1, !0), X(X(new vf, If, !0), Jf, !1, !0), X(X(X(new vf, If), Jf, !0), xf, !1, !0), X(X(X(new vf, Lf), Kf), xf, !0, !0));
function Mf() {
  W.call(this, void 0, !0);
  this.c = X(new vf, wf, !0, !0);
  if (this.ja) {
    if (this.c) {
      var a = this.c;
      a.va = this.ja;
      Cf(a);
    } else {
      this.ja.innerHTML = Wb(Yb);
    }
    R(this.ja, !!this.c);
  }
  Nf(this, Of);
}
z(Mf, W);
var Of = 0;
Mf.prototype.h = Of;
Mf.prototype.o = function() {
  delete this.h;
  Mf.m.o.call(this);
};
function Nf(a, b) {
  a.h = b;
  switch(b) {
    case 1:
      yf(a, "Screenshot");
      break;
    default:
      yf(a, "Taking Screenshot..."), zf(a, Xb("", null));
  }
}
;function Pf() {
  T.call(this);
}
z(Pf, T);
Pf.prototype.K = function() {
  this.b = this.a.D("DIV", "server-info");
  Qf(this);
};
function Qf(a, b, c, d) {
  var f = [];
  b && f.push(b);
  c && f.push("v" + c);
  d && f.push("r" + d);
  Ad(a.g(), f.length ? f.join("\u00a0\u00a0|\u00a0\u00a0") : "Server info unavailable");
}
;function Rf(a, b) {
  S.call(this);
  a && Sf(this, a, b);
}
z(Rf, S);
e = Rf.prototype;
e.Pa = null;
e.Db = null;
e.bc = null;
e.Eb = null;
e.da = -1;
e.ya = -1;
e.Lb = !1;
var Tf = {3:13, 12:144, 63232:38, 63233:40, 63234:37, 63235:39, 63236:112, 63237:113, 63238:114, 63239:115, 63240:116, 63241:117, 63242:118, 63243:119, 63244:120, 63245:121, 63246:122, 63247:123, 63248:44, 63272:46, 63273:36, 63275:35, 63276:33, 63277:34, 63289:144, 63302:45}, Uf = {Up:38, Down:40, Left:37, Right:39, Enter:13, F1:112, F2:113, F3:114, F4:115, F5:116, F6:117, F7:118, F8:119, F9:120, F10:121, F11:122, F12:123, "U+007F":46, Home:36, End:35, PageUp:33, PageDown:34, Insert:45}, Vf = F || 
I && J("525"), Wf = pb && H;
e = Rf.prototype;
e.sd = function(a) {
  I && (17 == this.da && !a.ctrlKey || 18 == this.da && !a.altKey || pb && 91 == this.da && !a.metaKey) && (this.ya = this.da = -1);
  -1 == this.da && (a.ctrlKey && 17 != a.a ? this.da = 17 : a.altKey && 18 != a.a ? this.da = 18 : a.metaKey && 91 != a.a && (this.da = 91));
  Vf && !Le(a.a, this.da, a.shiftKey, a.ctrlKey, a.altKey) ? this.handleEvent(a) : (this.ya = Ne(a.a), Wf && (this.Lb = a.altKey));
};
e.td = function(a) {
  this.ya = this.da = -1;
  this.Lb = a.altKey;
};
e.handleEvent = function(a) {
  var b = a.b, c, d, f = b.altKey;
  F && "keypress" == a.type ? c = this.ya : I && "keypress" == a.type ? c = this.ya : ob && !I ? c = this.ya : (c = b.keyCode || this.ya, d = b.charCode || 0, Wf && (f = this.Lb), pb && 63 == d && 224 == c && (c = 191));
  d = c = Ne(c);
  var g = b.keyIdentifier;
  c ? 63232 <= c && c in Tf ? d = Tf[c] : 25 == c && a.shiftKey && (d = 9) : g && g in Uf && (d = Uf[g]);
  this.da = d;
  a = new Xf(d, 0, 0, b);
  a.altKey = f;
  this.F(a);
};
e.g = function() {
  return this.Pa;
};
function Sf(a, b, c) {
  a.Eb && Yf(a);
  a.Pa = b;
  a.Db = M(a.Pa, "keypress", a, c);
  a.bc = M(a.Pa, "keydown", a.sd, c, a);
  a.Eb = M(a.Pa, "keyup", a.td, c, a);
}
function Yf(a) {
  a.Db && ($c(a.Db), $c(a.bc), $c(a.Eb), a.Db = null, a.bc = null, a.Eb = null);
  a.Pa = null;
  a.da = -1;
  a.ya = -1;
}
e.o = function() {
  Rf.m.o.call(this);
  Yf(this);
};
function Xf(a, b, c, d) {
  L.call(this, d);
  this.type = "key";
  this.a = a;
}
z(Xf, L);
function Zf() {
}
var $f;
aa(Zf);
var ag = {button:"pressed", checkbox:"checked", menuitem:"selected", menuitemcheckbox:"checked", menuitemradio:"checked", radio:"checked", tab:"selected", treeitem:"selected"};
e = Zf.prototype;
e.ub = function() {
};
e.Ta = function(a) {
  return a.a.D("DIV", bg(this, a).join(" "), a.Ga);
};
function cg(a, b, c) {
  if (a = a.g ? a.g() : a) {
    var d = [b];
    F && !J("7") && (d = dg(Fe(a), b), d.push(b));
    (c ? Ie : Ke)(a, d);
  }
}
e.yc = function(a) {
  ye(a) && this.Bc(a.g(), !0);
  a.isEnabled() && this.vb(a, !0);
};
e.Ac = function(a, b) {
  ge(a, !b, !F && !ob);
};
e.Bc = function(a, b) {
  cg(a, this.W() + "-rtl", b);
};
e.zc = function(a) {
  var b;
  return a.U & 32 && (b = a.g()) ? Ed(b) && Fd(b) : !1;
};
e.vb = function(a, b) {
  var c;
  if (a.U & 32 && (c = a.g())) {
    if (!b && a.R & 32) {
      try {
        c.blur();
      } catch (d) {
      }
      a.R & 32 && a.Cc();
    }
    (Ed(c) && Fd(c)) != b && Dd(c, b);
  }
};
e.Vb = function(a, b, c) {
  var d = a.g();
  if (d) {
    var f = eg(this, b);
    f && cg(a, f, c);
    this.qa(d, b, c);
  }
};
e.qa = function(a, b, c) {
  $f || ($f = {1:"disabled", 8:"selected", 16:"checked", 64:"expanded"});
  b = $f[b];
  var d = a.getAttribute("role") || null;
  d && (d = ag[d] || b, b = "checked" == b || "selected" == b ? d : b);
  b && Ee(a, b, c);
};
function fg(a, b) {
  if (a && (xd(a), b)) {
    if (v(b)) {
      Ad(a, b);
    } else {
      var c = function(b) {
        if (b) {
          var c = P(a);
          a.appendChild(v(b) ? c.createTextNode(b) : b);
        }
      };
      t(b) ? D(b, c) : !ca(b) || "nodeType" in b ? c(b) : D(Wa(b), c);
    }
  }
}
e.W = function() {
  return "goog-control";
};
function bg(a, b) {
  var c = a.W(), d = [c], f = a.W();
  f != c && d.push(f);
  c = b.R;
  for (f = [];c;) {
    var g = c & -c;
    f.push(eg(a, g));
    c &= ~g;
  }
  d.push.apply(d, f);
  (c = b.tc) && d.push.apply(d, c);
  F && !J("7") && d.push.apply(d, dg(d));
  return d;
}
function dg(a, b) {
  var c = [];
  b && (a = a.concat([b]));
  D([], function(d) {
    !Sa(d, ia(Ta, a)) || b && !Ta(d, b) || c.push(d.join("_"));
  });
  return c;
}
function eg(a, b) {
  if (!a.a) {
    var c = a.W();
    c.replace(/\xa0|\s/g, " ");
    a.a = {1:c + "-disabled", 2:c + "-hover", 4:c + "-active", 8:c + "-selected", 16:c + "-checked", 32:c + "-focused", 64:c + "-open"};
  }
  return a.a[b];
}
;function gg(a, b) {
  if (!a) {
    throw Error("Invalid class name " + a);
  }
  if (!w(b)) {
    throw Error("Invalid decorator function " + b);
  }
}
var hg = {};
function Y(a, b, c) {
  T.call(this, c);
  if (!b) {
    b = this.constructor;
    for (var d;b;) {
      d = x(b);
      if (d = hg[d]) {
        break;
      }
      b = b.m ? b.m.constructor : null;
    }
    b = d ? w(d.Ea) ? d.Ea() : new d : null;
  }
  this.c = b;
  this.Ga = n(a) ? a : null;
}
z(Y, T);
e = Y.prototype;
e.Ga = null;
e.R = 0;
e.U = 39;
e.Mb = 255;
e.Ia = 0;
e.tc = null;
e.Zb = !0;
function ig(a, b) {
  a.L && b != a.Zb && jg(a, b);
  a.Zb = b;
}
e.K = function() {
  var a = this.c.Ta(this);
  this.b = a;
  var b = this.c.ub();
  if (b) {
    var c = a.getAttribute("role") || null;
    b != c && De(a, b);
  }
  this.c.Ac(a, !1);
};
e.Fa = function() {
  return this.g();
};
e.T = function() {
  Y.m.T.call(this);
  var a = this.c, b = this.b;
  this.isEnabled() || a.qa(b, 1, !this.isEnabled());
  this.U & 8 && a.qa(b, 8, !!(this.R & 8));
  this.U & 16 && a.qa(b, 16, !!(this.R & 16));
  this.U & 64 && a.qa(b, 64, !!(this.R & 64));
  this.c.yc(this);
  this.U & -2 && (this.Zb && jg(this, !0), this.U & 32 && (a = this.g())) && (b = this.h || (this.h = new Rf), Sf(b, a), U(this).v(b, "key", this.eb).v(a, "focus", this.hd).v(a, "blur", this.Cc));
};
function jg(a, b) {
  var c = U(a), d = a.g();
  b ? (c.v(d, "mouseover", a.Xb).v(d, "mousedown", a.wb).v(d, "mouseup", a.zb).v(d, "mouseout", a.$b), a.gb != q && c.v(d, "contextmenu", a.gb), F && (c.v(d, "dblclick", a.Fc), a.A || (a.A = new kg(a), ra(a, ia(B, a.A))))) : (c.ia(d, "mouseover", a.Xb).ia(d, "mousedown", a.wb).ia(d, "mouseup", a.zb).ia(d, "mouseout", a.$b), a.gb != q && c.ia(d, "contextmenu", a.gb), F && (c.ia(d, "dblclick", a.Fc), B(a.A), a.A = null));
}
e.ba = function() {
  Y.m.ba.call(this);
  this.h && Yf(this.h);
  this.isEnabled() && this.c.vb(this, !1);
};
e.o = function() {
  Y.m.o.call(this);
  this.h && (this.h.N(), delete this.h);
  delete this.c;
  this.A = this.tc = this.Ga = null;
};
function lg(a) {
  a = a.Ga;
  if (!a) {
    return "";
  }
  if (!v(a)) {
    if (t(a)) {
      a = Qa(a, Gd).join("");
    } else {
      if (id && "innerText" in a) {
        a = a.innerText.replace(/(\r\n|\r|\n)/g, "\n");
      } else {
        var b = [];
        Hd(a, b, !0);
        a = b.join("");
      }
      a = a.replace(/ \xAD /g, " ").replace(/\xAD/g, "");
      a = a.replace(/\u200B/g, "");
      id || (a = a.replace(/ +/g, " "));
      " " != a && (a = a.replace(/^\s*/, ""));
    }
  }
  return a.replace(/[\t\r\n ]+/g, " ").replace(/^[\t\r\n ]+|[\t\r\n ]+$/g, "");
}
e.isEnabled = function() {
  return !(this.R & 1);
};
e.ea = function(a) {
  var b = this.j;
  b && "function" == typeof b.isEnabled && !b.isEnabled() || !mg(this, 1, !a) || (a || (ng(this, !1), og(this, !1)), this.c.vb(this, a), pg(this, 1, !a, !0));
};
function og(a, b) {
  mg(a, 2, b) && pg(a, 2, b);
}
function ng(a, b) {
  mg(a, 4, b) && pg(a, 4, b);
}
function qg(a, b) {
  mg(a, 8, b) && pg(a, 8, b);
}
function rg(a, b) {
  mg(a, 64, b) && pg(a, 64, b);
}
function pg(a, b, c, d) {
  d || 1 != b ? a.U & b && c != !!(a.R & b) && (a.c.Vb(a, b, c), a.R = c ? a.R | b : a.R & ~b) : a.ea(!c);
}
function sg(a, b, c) {
  if (a.L && a.R & b && !c) {
    throw Error("Component already rendered");
  }
  !c && a.R & b && pg(a, b, !1);
  a.U = c ? a.U | b : a.U & ~b;
}
function Z(a, b) {
  return !!(a.Mb & b) && !!(a.U & b);
}
function mg(a, b, c) {
  return !!(a.U & b) && !!(a.R & b) != c && (!(a.Ia & b) || a.F(re(b, c))) && !a.ra;
}
e.Xb = function(a) {
  (!a.h || !zd(this.g(), a.h)) && this.F("enter") && this.isEnabled() && Z(this, 2) && og(this, !0);
};
e.$b = function(a) {
  a.h && zd(this.g(), a.h) || !this.F("leave") || (Z(this, 4) && ng(this, !1), Z(this, 2) && og(this, !1));
};
e.gb = q;
e.wb = function(a) {
  this.isEnabled() && (Z(this, 2) && og(this, !0), Fc(a) && (Z(this, 4) && ng(this, !0), this.c && this.c.zc(this) && this.g().focus()));
  Fc(a) && a.j();
};
e.zb = function(a) {
  this.isEnabled() && (Z(this, 2) && og(this, !0), this.R & 4 && this.jb(a) && Z(this, 4) && ng(this, !1));
};
e.Fc = function(a) {
  this.isEnabled() && this.jb(a);
};
e.jb = function(a) {
  if (Z(this, 16)) {
    var b = !(this.R & 16);
    mg(this, 16, b) && pg(this, 16, b);
  }
  Z(this, 8) && qg(this, !0);
  Z(this, 64) && rg(this, !(this.R & 64));
  b = new K("action", this);
  a && (b.altKey = a.altKey, b.ctrlKey = a.ctrlKey, b.metaKey = a.metaKey, b.shiftKey = a.shiftKey, b.B = a.B);
  return this.F(b);
};
e.hd = function() {
  Z(this, 32) && mg(this, 32, !0) && pg(this, 32, !0);
};
e.Cc = function() {
  Z(this, 4) && ng(this, !1);
  Z(this, 32) && mg(this, 32, !1) && pg(this, 32, !1);
};
e.eb = function(a) {
  return this.isEnabled() && this.Wb(a) ? (a.j(), a.w(), !0) : !1;
};
e.Wb = function(a) {
  return 13 == a.a && this.jb(a);
};
if (!w(Y)) {
  throw Error("Invalid component class " + Y);
}
if (!w(Zf)) {
  throw Error("Invalid renderer class " + Zf);
}
var tg = x(Y);
hg[tg] = Zf;
gg("goog-control", function() {
  return new Y(null);
});
function kg(a) {
  this.b = a;
  this.a = !1;
  this.c = new le(this);
  ra(this, ia(B, this.c));
  a = this.b.b;
  this.c.v(a, "mousedown", this.h).v(a, "mouseup", this.l).v(a, "click", this.j);
}
z(kg, oa);
kg.prototype.h = function() {
  this.a = !1;
};
kg.prototype.l = function() {
  this.a = !0;
};
kg.prototype.j = function(a) {
  if (this.a) {
    this.a = !1;
  } else {
    var b = a.b, c = b.button, d = b.type;
    b.button = 0;
    b.type = "mousedown";
    this.b.wb(new L(b, a.c));
    b.type = "mouseup";
    this.b.zb(new L(b, a.c));
    b.button = c;
    b.type = d;
  }
};
kg.prototype.o = function() {
  this.b = null;
  kg.m.o.call(this);
};
function ug() {
}
z(ug, Zf);
aa(ug);
e = ug.prototype;
e.W = function() {
  return "goog-tab";
};
e.ub = function() {
  return "tab";
};
e.Ta = function(a) {
  var b = ug.m.Ta.call(this, a);
  (a = a.Oa()) && this.oa(b, a);
  return b;
};
e.Oa = function(a) {
  return a.title || "";
};
e.oa = function(a, b) {
  a && (a.title = b || "");
};
function vg(a, b, c) {
  Y.call(this, a, b || ug.Ea(), c);
  sg(this, 8, !0);
  this.Ia |= 9;
}
z(vg, Y);
vg.prototype.Oa = function() {
  return this.l;
};
vg.prototype.oa = function(a) {
  this.c.oa(this.g(), a);
  this.Vc(a);
};
vg.prototype.Vc = function(a) {
  this.l = a;
};
gg("goog-tab", function() {
  return new vg(null);
});
function wg(a) {
  this.b = a;
}
aa(wg);
function xg(a, b) {
  a && (a.tabIndex = b ? 0 : -1);
}
function yg(a, b) {
  var c = b.g();
  ge(c, !0, H);
  F && (c.hideFocus = !0);
  var d = a.b;
  d && De(c, d);
}
wg.prototype.W = function() {
  return "goog-container";
};
wg.prototype.a = function(a) {
  var b = this.W(), c = [b, a.za == zg ? b + "-horizontal" : b + "-vertical"];
  a.isEnabled() || c.push(b + "-disabled");
  return c;
};
function Ag(a, b, c) {
  T.call(this, c);
  this.tb = b || wg.Ea();
  this.za = a || Bg;
}
z(Ag, T);
var zg = "horizontal", Bg = "vertical";
e = Ag.prototype;
e.cc = null;
e.bb = null;
e.tb = null;
e.za = null;
e.cb = !0;
e.Sa = !0;
e.J = -1;
e.P = null;
e.Va = !1;
e.la = null;
function Cg(a) {
  return a.cc || a.g();
}
e.K = function() {
  this.b = this.a.D("DIV", this.tb.a(this).join(" "));
};
e.Fa = function() {
  return this.g();
};
e.T = function() {
  Ag.m.T.call(this);
  ve(this, function(a) {
    a.L && Dg(this, a);
  }, this);
  var a = this.g();
  yg(this.tb, this);
  Eg(this, this.cb);
  U(this).v(this, "enter", this.od).v(this, "highlight", this.rd).v(this, "unhighlight", this.Ad).v(this, "open", this.vd).v(this, "close", this.md).v(a, "mousedown", this.gd).v(P(a), "mouseup", this.nd).v(a, ["mousedown", "mouseup", "mouseover", "mouseout", "contextmenu"], this.ld);
  Fg(this);
};
function Fg(a) {
  var b = U(a), c = Cg(a);
  b.v(c, "focus", a.xc).v(c, "blur", a.ed).v(a.bb || (a.bb = new Rf(Cg(a))), "key", a.fd);
}
e.ba = function() {
  Gg(this, -1);
  this.P && rg(this.P, !1);
  this.Va = !1;
  Ag.m.ba.call(this);
};
e.o = function() {
  Ag.m.o.call(this);
  this.bb && (this.bb.N(), this.bb = null);
  this.tb = this.P = this.la = this.cc = null;
};
e.od = function() {
  return !0;
};
e.rd = function(a) {
  var b = ze(this, a.target);
  if (-1 < b && b != this.J) {
    var c = V(this, this.J);
    c && og(c, !1);
    this.J = b;
    c = V(this, this.J);
    this.Va && ng(c, !0);
    this.P && c != this.P && (c.U & 64 ? rg(c, !0) : rg(this.P, !1));
  }
  b = this.g();
  null != a.target.g() && Ee(b, "activedescendant", a.target.g().id);
};
e.Ad = function(a) {
  a.target == V(this, this.J) && (this.J = -1);
  this.g().removeAttribute("aria-activedescendant");
};
e.vd = function(a) {
  (a = a.target) && a != this.P && a.j == this && (this.P && rg(this.P, !1), this.P = a);
};
e.md = function(a) {
  a.target == this.P && (this.P = null);
};
e.gd = function(a) {
  this.Sa && (this.Va = !0);
  var b = Cg(this);
  b && Ed(b) && Fd(b) ? b.focus() : a.j();
};
e.nd = function() {
  this.Va = !1;
};
e.ld = function(a) {
  var b;
  a: {
    b = a.target;
    if (this.la) {
      for (var c = this.g();b && b !== c;) {
        var d = b.id;
        if (d in this.la) {
          b = this.la[d];
          break a;
        }
        b = b.parentNode;
      }
    }
    b = null;
  }
  if (b) {
    switch(a.type) {
      case "mousedown":
        b.wb(a);
        break;
      case "mouseup":
        b.zb(a);
        break;
      case "mouseover":
        b.Xb(a);
        break;
      case "mouseout":
        b.$b(a);
        break;
      case "contextmenu":
        b.gb(a);
    }
  }
};
e.xc = function() {
};
e.ed = function() {
  Gg(this, -1);
  this.Va = !1;
  this.P && rg(this.P, !1);
};
e.fd = function(a) {
  return this.isEnabled() && this.cb && (0 != we(this) || this.cc) && Hg(this, a) ? (a.j(), a.w(), !0) : !1;
};
function Hg(a, b) {
  var c = V(a, a.J);
  if (c && "function" == typeof c.eb && c.eb(b) || a.P && a.P != c && "function" == typeof a.P.eb && a.P.eb(b)) {
    return !0;
  }
  if (b.shiftKey || b.ctrlKey || b.metaKey || b.altKey) {
    return !1;
  }
  switch(b.a) {
    case 27:
      Cg(a).blur();
      break;
    case 36:
      Ig(a);
      break;
    case 35:
      Jg(a);
      break;
    case 38:
      if (a.za == Bg) {
        Kg(a);
      } else {
        return !1;
      }
      break;
    case 37:
      if (a.za == zg) {
        ye(a) ? Lg(a) : Kg(a);
      } else {
        return !1;
      }
      break;
    case 40:
      if (a.za == Bg) {
        Lg(a);
      } else {
        return !1;
      }
      break;
    case 39:
      if (a.za == zg) {
        ye(a) ? Kg(a) : Lg(a);
      } else {
        return !1;
      }
      break;
    default:
      return !1;
  }
  return !0;
}
function Dg(a, b) {
  var c = b.g(), c = c.id || (c.id = b.O());
  a.la || (a.la = {});
  a.la[c] = b;
}
e.ga = function(a, b) {
  Ag.m.ga.call(this, a, b);
};
e.ob = function(a, b, c) {
  a.Ia |= 2;
  a.Ia |= 64;
  sg(a, 32, !1);
  ig(a, !1);
  var d = a.j == this ? ze(this, a) : -1;
  Ag.m.ob.call(this, a, b, c);
  a.L && this.L && Dg(this, a);
  a = d;
  -1 == a && (a = we(this));
  a == this.J ? this.J = Math.min(we(this) - 1, b) : a > this.J && b <= this.J ? this.J++ : a < this.J && b > this.J && this.J--;
};
e.removeChild = function(a, b) {
  if (a = v(a) ? te(this, a) : a) {
    var c = ze(this, a);
    -1 != c && (c == this.J ? (og(a, !1), this.J = -1) : c < this.J && this.J--);
    var d = a.g();
    d && d.id && this.la && (c = this.la, d = d.id, d in c && delete c[d]);
  }
  a = Ag.m.removeChild.call(this, a, b);
  ig(a, !0);
  return a;
};
function Eg(a, b) {
  a.cb = b;
  var c = a.g();
  c && (R(c, b), xg(Cg(a), a.Sa && a.cb));
}
e.isEnabled = function() {
  return this.Sa;
};
e.ea = function(a) {
  this.Sa != a && this.F(a ? "enable" : "disable") && (a ? (this.Sa = !0, ve(this, function(a) {
    a.ad ? delete a.ad : a.ea(!0);
  })) : (ve(this, function(a) {
    a.isEnabled() ? a.ea(!1) : a.ad = !0;
  }), this.Va = this.Sa = !1), xg(Cg(this), a && this.cb));
};
function Gg(a, b) {
  var c = V(a, b);
  c ? og(c, !0) : -1 < a.J && og(V(a, a.J), !1);
}
function Ig(a) {
  Mg(a, function(a, c) {
    return (a + 1) % c;
  }, we(a) - 1);
}
function Jg(a) {
  Mg(a, function(a, c) {
    a--;
    return 0 > a ? c - 1 : a;
  }, 0);
}
function Lg(a) {
  Mg(a, function(a, c) {
    return (a + 1) % c;
  }, a.J);
}
function Kg(a) {
  Mg(a, function(a, c) {
    a--;
    return 0 > a ? c - 1 : a;
  }, a.J);
}
function Mg(a, b, c) {
  c = 0 > c ? ze(a, a.P) : c;
  var d = we(a);
  c = b.call(a, c, d);
  for (var f = 0;f <= d;) {
    var g = V(a, c);
    if (g && g.isEnabled() && g.U & 2) {
      a.ic(c);
      break;
    }
    f++;
    c = b.call(a, c, d);
  }
}
e.ic = function(a) {
  Gg(this, a);
};
function Ng() {
  this.b = "tablist";
}
z(Ng, wg);
aa(Ng);
Ng.prototype.W = function() {
  return "goog-tab-bar";
};
Ng.prototype.a = function(a) {
  var b = Ng.m.a.call(this, a);
  if (!this.c) {
    var c = this.W();
    this.c = db(Og, c + "-top", Pg, c + "-bottom", Qg, c + "-start", Rg, c + "-end");
  }
  b.push(this.c[a.c]);
  return b;
};
function Sg(a, b, c) {
  a = a || Og;
  if (this.g()) {
    throw Error("Component already rendered");
  }
  this.za = a == Qg || a == Rg ? Bg : zg;
  this.c = a;
  Ag.call(this, this.za, b || Ng.Ea(), c);
  Tg(this);
}
z(Sg, Ag);
var Og = "top", Pg = "bottom", Qg = "start", Rg = "end";
e = Sg.prototype;
e.Y = null;
e.T = function() {
  Sg.m.T.call(this);
  Tg(this);
};
e.o = function() {
  Sg.m.o.call(this);
  this.Y = null;
};
e.removeChild = function(a, b) {
  Ug(this, a);
  return Sg.m.removeChild.call(this, a, b);
};
e.ic = function(a) {
  Sg.m.ic.call(this, a);
  Vg(this, V(this, a));
};
function Vg(a, b) {
  b ? qg(b, !0) : a.Y && qg(a.Y, !1);
}
function Ug(a, b) {
  if (b && b == a.Y) {
    for (var c = ze(a, b), d = c - 1;b = V(a, d);d--) {
      if (b.isEnabled()) {
        Vg(a, b);
        return;
      }
    }
    for (c += 1;b = V(a, c);c++) {
      if (b.isEnabled()) {
        Vg(a, b);
        return;
      }
    }
    Vg(a, null);
  }
}
e.yd = function(a) {
  this.Y && this.Y != a.target && qg(this.Y, !1);
  this.Y = a.target;
};
e.zd = function(a) {
  a.target == this.Y && (this.Y = null);
};
e.wd = function(a) {
  Ug(this, a.target);
};
e.xd = function(a) {
  Ug(this, a.target);
};
e.xc = function() {
  V(this, this.J) || Gg(this, ze(this, this.Y || V(this, 0)));
};
function Tg(a) {
  U(a).v(a, "select", a.yd).v(a, "unselect", a.zd).v(a, "disable", a.wd).v(a, "hide", a.xd);
}
gg("goog-tab-bar", function() {
  return new Sg;
});
function Wg() {
  T.call(this);
}
z(Wg, T);
Wg.prototype.h = null;
Wg.prototype.o = function() {
  delete this.h;
  Wg.m.o.call(this);
};
Wg.prototype.K = function() {
  this.b = this.a.D("DIV", "control-block");
  this.h && (D(this.h, this.c, this), this.h = null);
};
Wg.prototype.c = function(a) {
  var b = this.g();
  b ? (b.childNodes.length && b.appendChild(this.a.a.createTextNode("\u00a0\u00a0|\u00a0\u00a0")), b.appendChild(a)) : (this.h || (this.h = []), this.h.push(a));
};
function Xg(a) {
  W.call(this, void 0, !0);
  yf(this, a);
  M(this, Hf, this.Ba, !1, this);
}
z(Xg, W);
Xg.prototype.K = function() {
  Xg.m.K.call(this);
  var a = this.Fa(), b = this.qc();
  a.appendChild(b);
};
Xg.prototype.X = function(a) {
  Xg.m.X.call(this, a);
  a && this.F("show");
};
Xg.prototype.Ba = function(a) {
  "ok" == a.key && this.Ic() && this.F("action");
};
function Yg(a) {
  Xg.call(this, "Create a New Session");
  this.h = Qa(a, function(a) {
    return v(a) ? {browserName:a} : a;
  });
  M(this, "show", this.Qd, !1, this);
}
z(Yg, Xg);
e = Yg.prototype;
e.Ca = null;
e.o = function() {
  delete this.h;
  delete this.Ca;
  Yg.m.o.call(this);
};
e.qc = function() {
  function a(a) {
    var d = a.browserName;
    (a = a.version) && (d += " " + a);
    return b.D("OPTION", null, d);
  }
  var b = this.a;
  this.Ca = b.D("SELECT", null, a(""));
  D(this.h, function(b) {
    b = a(b);
    this.Ca.appendChild(b);
  }, this);
  return b.D("LABEL", null, "Browser:\u00a0", this.Ca);
};
e.Tb = function() {
  return this.h[this.Ca.selectedIndex - 1];
};
e.Ic = function() {
  return !!this.Ca.selectedIndex;
};
e.Qd = function() {
  this.Ca.selectedIndex = 0;
};
function Zg(a) {
  T.call(this);
  this.S = a;
}
z(Zg, T);
Zg.prototype.o = function() {
  delete this.S;
  Zg.m.o.call(this);
};
Zg.prototype.K = function() {
  var a = this.a;
  this.b = a.D("FIELDSET", null, a.D("LEGEND", null, this.S), this.rc());
};
Zg.prototype.rc = function() {
  return null;
};
function $g() {
}
z($g, Zf);
aa($g);
e = $g.prototype;
e.ub = function() {
  return "button";
};
e.qa = function(a, b, c) {
  switch(b) {
    case 8:
    ;
    case 16:
      Ee(a, "pressed", c);
      break;
    default:
    ;
    case 64:
    ;
    case 1:
      $g.m.qa.call(this, a, b, c);
  }
};
e.Ta = function(a) {
  var b = $g.m.Ta.call(this, a);
  this.oa(b, a.Oa());
  var c = a.H;
  c && this.wc(b, c);
  a.U & 16 && this.qa(b, 16, !!(a.R & 16));
  return b;
};
e.wc = q;
e.Oa = function(a) {
  return a.title;
};
e.oa = function(a, b) {
  a && (b ? a.title = b : a.removeAttribute("title"));
};
e.W = function() {
  return "goog-button";
};
function ah() {
}
z(ah, $g);
aa(ah);
e = ah.prototype;
e.ub = function() {
};
e.Ta = function(a) {
  ig(a, !1);
  a.Mb &= -256;
  sg(a, 32, !1);
  return a.a.D("BUTTON", {"class":bg(this, a).join(" "), disabled:!a.isEnabled(), title:a.Oa() || "", value:a.H || ""}, lg(a) || "");
};
e.yc = function(a) {
  U(a).v(a.g(), "click", a.jb);
};
e.Ac = q;
e.Bc = q;
e.zc = function(a) {
  return a.isEnabled();
};
e.vb = q;
e.Vb = function(a, b, c) {
  ah.m.Vb.call(this, a, b, c);
  (a = a.g()) && 1 == b && (a.disabled = c);
};
e.wc = function(a, b) {
  a && (a.value = b);
};
e.qa = q;
function bh(a, b, c) {
  Y.call(this, a, b || ah.Ea(), c);
}
z(bh, Y);
e = bh.prototype;
e.Oa = function() {
  return this.G;
};
e.oa = function(a) {
  this.G = a;
  this.c.oa(this.g(), a);
};
e.Vc = function(a) {
  this.G = a;
};
e.o = function() {
  bh.m.o.call(this);
  delete this.H;
  delete this.G;
};
e.T = function() {
  bh.m.T.call(this);
  if (this.U & 32) {
    var a = this.g();
    a && U(this).v(a, "keyup", this.Wb);
  }
};
e.Wb = function(a) {
  return 13 == a.a && "key" == a.type || 32 == a.a && "keyup" == a.type ? this.jb(a) : 32 == a.a;
};
gg("goog-button", function() {
  return new bh(null);
});
function ch(a) {
  a = String(a);
  if (/^\s*$/.test(a) ? 0 : /^[\],:{}\s\u2028\u2029]*$/.test(a.replace(/\\["\\\/bfnrtu]/g, "@").replace(/"[^"\\\n\r\u2028\u2029\x00-\x08\x0a-\x1f]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:[\s\u2028\u2029]*\[)+/g, ""))) {
    try {
      return eval("(" + a + ")");
    } catch (b) {
    }
  }
  throw Error("Invalid JSON string: " + a);
}
function dh() {
}
function eh(a, b) {
  var c = [];
  fh(a, b, c);
  return c.join("");
}
function fh(a, b, c) {
  if (null == b) {
    c.push("null");
  } else {
    if ("object" == typeof b) {
      if (t(b)) {
        var d = b;
        b = d.length;
        c.push("[");
        for (var f = "", g = 0;g < b;g++) {
          c.push(f), fh(a, d[g], c), f = ",";
        }
        c.push("]");
        return;
      }
      if (b instanceof String || b instanceof Number || b instanceof Boolean) {
        b = b.valueOf();
      } else {
        c.push("{");
        f = "";
        for (d in b) {
          Object.prototype.hasOwnProperty.call(b, d) && (g = b[d], "function" != typeof g && (c.push(f), gh(d, c), c.push(":"), fh(a, g, c), f = ","));
        }
        c.push("}");
        return;
      }
    }
    switch(typeof b) {
      case "string":
        gh(b, c);
        break;
      case "number":
        c.push(isFinite(b) && !isNaN(b) ? b : "null");
        break;
      case "boolean":
        c.push(b);
        break;
      case "function":
        break;
      default:
        throw Error("Unknown type: " + typeof b);;
    }
  }
}
var hh = {'"':'\\"', "\\":"\\\\", "/":"\\/", "\b":"\\b", "\f":"\\f", "\n":"\\n", "\r":"\\r", "\t":"\\t", "\x0B":"\\u000b"}, ih = /\uffff/.test("\uffff") ? /[\\\"\x00-\x1f\x7f-\uffff]/g : /[\\\"\x00-\x1f\x7f-\xff]/g;
function gh(a, b) {
  b.push('"', a.replace(ih, function(a) {
    var b = hh[a];
    b || (b = "\\u" + (a.charCodeAt(0) | 65536).toString(16).substr(1), hh[a] = b);
    return b;
  }), '"');
}
;function jh(a, b) {
  null != a && this.a.apply(this, arguments);
}
jh.prototype.b = "";
jh.prototype.a = function(a, b, c) {
  this.b += a;
  if (null != b) {
    for (var d = 1;d < arguments.length;d++) {
      this.b += arguments[d];
    }
  }
  return this;
};
jh.prototype.clear = function() {
  this.b = "";
};
jh.prototype.toString = function() {
  return this.b;
};
function kh(a, b) {
  var c = Array.prototype.slice.call(arguments), d = c.shift();
  if ("undefined" == typeof d) {
    throw Error("[goog.string.format] Template required");
  }
  return d.replace(/%([0\-\ \+]*)(\d+)?(\.(\d+))?([%sfdiu])/g, function(a, b, d, k, l, r, p, u) {
    if ("%" == r) {
      return "%";
    }
    var G = c.shift();
    if ("undefined" == typeof G) {
      throw Error("[goog.string.format] Not enough arguments");
    }
    arguments[0] = G;
    return lh[r].apply(null, arguments);
  });
}
var lh = {s:function(a, b, c) {
  return isNaN(c) || "" == c || a.length >= c ? a : a = -1 < b.indexOf("-", 0) ? a + Fa(" ", c - a.length) : Fa(" ", c - a.length) + a;
}, f:function(a, b, c, d, f) {
  d = a.toString();
  isNaN(f) || "" == f || (d = parseFloat(a).toFixed(f));
  var g;
  g = 0 > a ? "-" : 0 <= b.indexOf("+") ? "+" : 0 <= b.indexOf(" ") ? " " : "";
  0 <= a && (d = g + d);
  if (isNaN(c) || d.length >= c) {
    return d;
  }
  d = isNaN(f) ? Math.abs(a).toString() : Math.abs(a).toFixed(f);
  a = c - d.length - g.length;
  return d = 0 <= b.indexOf("-", 0) ? g + d + Fa(" ", a) : g + Fa(0 <= b.indexOf("0", 0) ? "0" : " ", a) + d;
}, d:function(a, b, c, d, f, g, h, k) {
  return lh.f(parseInt(a, 10), b, c, d, 0, g, h, k);
}};
lh.i = lh.d;
lh.u = lh.d;
function mh() {
  this.a = new dh;
}
function nh(a) {
  var b = new mh;
  if (null == a) {
    return "";
  }
  if (v(a)) {
    if (/^[\s\xa0]*$/.test(a)) {
      return "";
    }
    a = ch(a);
  }
  var c = new jh;
  oh(b, a, c, 0);
  return c.toString();
}
function oh(a, b, c, d) {
  var f = ba(b);
  switch(f) {
    case "null":
    ;
    case "boolean":
    ;
    case "number":
    ;
    case "string":
      c.a(kh("", f), eh(a.a, b), kh("", f));
      break;
    case "array":
      c.a("[");
      for (var g = 0, g = 0;g < b.length;g++) {
        0 < g && c.a(","), c.a("\n"), c.a(Fa(" ", d + 2)), oh(a, b[g], c, d + 2);
      }
      0 < g && (c.a("\n"), c.a(Fa(" ", d)));
      c.a("]");
      break;
    case "object":
      c.a("{");
      f = 0;
      for (g in b) {
        b.hasOwnProperty(g) && (0 < f && c.a(","), c.a("\n"), c.a(Fa(" ", d + 2)), c.a("", eh(a.a, g), ""), c.a(":", " "), oh(a, b[g], c, d + 2), f++);
      }
      0 < f && (c.a("\n"), c.a(Fa(" ", d)));
      c.a("}");
      break;
    default:
      c.a(kh("", "unknown"), eh(a.a, ""), kh("", "unknown"));
  }
}
;function ph(a, b, c, d, f, g, h, k) {
  var l, r;
  if (l = c.offsetParent) {
    var p = "HTML" == l.tagName || "BODY" == l.tagName;
    p && "static" == Sd(l, "position") || (r = Zd(l), p || (p = (p = ee(l)) && H ? -l.scrollLeft : !p || F && J("8") || "visible" == Sd(l, "overflowX") ? l.scrollLeft : l.scrollWidth - l.clientWidth - l.scrollLeft, r = jd(r, new N(p, l.scrollTop))));
  }
  l = r || new N;
  r = ce(a);
  if (p = Yd(a)) {
    var u = new Md(p.left, p.top, p.right - p.left, p.bottom - p.top), p = Math.max(r.left, u.left), G = Math.min(r.left + r.width, u.left + u.width);
    if (p <= G) {
      var ta = Math.max(r.top, u.top), u = Math.min(r.top + r.height, u.top + u.height);
      ta <= u && (r.left = p, r.top = ta, r.width = G - p, r.height = u - ta);
    }
  }
  p = O(a);
  ta = O(c);
  if (p.a != ta.a) {
    var G = p.a.body, ta = rd(ta.a), u = new N(0, 0), Na = qd(P(G)), xe = G;
    do {
      var bb;
      Na == ta ? bb = Zd(xe) : (bb = Wd(xe), bb = new N(bb.left, bb.top));
      u.x += bb.x;
      u.y += bb.y;
    } while (Na && Na != ta && Na != Na.parent && (xe = Na.frameElement) && (Na = Na.parent));
    G = jd(u, Zd(G));
    !F || ub(9) || Id(p) || (G = jd(G, Jd(p)));
    r.left += G.x;
    r.top += G.y;
  }
  a = qh(a, b);
  b = new N(a & 2 ? r.left + r.width : r.left, a & 1 ? r.top + r.height : r.top);
  b = jd(b, l);
  f && (b.x += (a & 2 ? -1 : 1) * f.x, b.y += (a & 1 ? -1 : 1) * f.y);
  var ka;
  h && (ka = Yd(c)) && (ka.top -= l.y, ka.right -= l.x, ka.bottom -= l.y, ka.left -= l.x);
  return rh(b, c, d, g, ka, h, k);
}
function rh(a, b, c, d, f, g, h) {
  a = a.clone();
  var k = qh(b, c);
  c = ae(b);
  h = h ? h.clone() : c.clone();
  a = a.clone();
  h = h.clone();
  var l = 0;
  if (d || 0 != k) {
    k & 2 ? a.x -= h.width + (d ? d.right : 0) : d && (a.x += d.left), k & 1 ? a.y -= h.height + (d ? d.bottom : 0) : d && (a.y += d.top);
  }
  if (g) {
    if (f) {
      d = a;
      k = h;
      l = 0;
      65 == (g & 65) && (d.x < f.left || d.x >= f.right) && (g &= -2);
      132 == (g & 132) && (d.y < f.top || d.y >= f.bottom) && (g &= -5);
      d.x < f.left && g & 1 && (d.x = f.left, l |= 1);
      if (g & 16) {
        var r = d.x;
        d.x < f.left && (d.x = f.left, l |= 4);
        d.x + k.width > f.right && (k.width = Math.min(f.right - d.x, r + k.width - f.left), k.width = Math.max(k.width, 0), l |= 4);
      }
      d.x + k.width > f.right && g & 1 && (d.x = Math.max(f.right - k.width, f.left), l |= 1);
      g & 2 && (l = l | (d.x < f.left ? 16 : 0) | (d.x + k.width > f.right ? 32 : 0));
      d.y < f.top && g & 4 && (d.y = f.top, l |= 2);
      g & 32 && (r = d.y, d.y < f.top && (d.y = f.top, l |= 8), d.y + k.height > f.bottom && (k.height = Math.min(f.bottom - d.y, r + k.height - f.top), k.height = Math.max(k.height, 0), l |= 8));
      d.y + k.height > f.bottom && g & 4 && (d.y = Math.max(f.bottom - k.height, f.top), l |= 2);
      g & 8 && (l = l | (d.y < f.top ? 64 : 0) | (d.y + k.height > f.bottom ? 128 : 0));
      f = l;
    } else {
      f = 256;
    }
    l = f;
  }
  g = new Md(0, 0, 0, 0);
  g.left = a.x;
  g.top = a.y;
  g.width = h.width;
  g.height = h.height;
  f = l;
  if (f & 496) {
    return f;
  }
  Td(b, new N(g.left, g.top));
  h = new kd(g.width, g.height);
  c == h || c && h && c.width == h.width && c.height == h.height || (c = h, a = Id(O(P(b))), !F || J("10") || a && J("8") ? (b = b.style, H ? b.MozBoxSizing = "border-box" : I ? b.WebkitBoxSizing = "border-box" : b.boxSizing = "border-box", b.width = Math.max(c.width, 0) + "px", b.height = Math.max(c.height, 0) + "px") : (h = b.style, a ? (F ? (a = ie(b, "paddingLeft"), g = ie(b, "paddingRight"), d = ie(b, "paddingTop"), k = ie(b, "paddingBottom"), a = new Q(d, g, k, a)) : (a = Rd(b, "paddingLeft"), 
  g = Rd(b, "paddingRight"), d = Rd(b, "paddingTop"), k = Rd(b, "paddingBottom"), a = new Q(parseFloat(d), parseFloat(g), parseFloat(k), parseFloat(a))), F && !ub(9) ? (g = ke(b, "borderLeft"), d = ke(b, "borderRight"), k = ke(b, "borderTop"), b = ke(b, "borderBottom"), b = new Q(k, d, b, g)) : (g = Rd(b, "borderLeftWidth"), d = Rd(b, "borderRightWidth"), k = Rd(b, "borderTopWidth"), b = Rd(b, "borderBottomWidth"), b = new Q(parseFloat(k), parseFloat(d), parseFloat(b), parseFloat(g))), h.pixelWidth = 
  c.width - b.left - a.left - a.right - b.right, h.pixelHeight = c.height - b.top - a.top - a.bottom - b.bottom) : (h.pixelWidth = c.width, h.pixelHeight = c.height)));
  return f;
}
function qh(a, b) {
  return (b & 4 && ee(a) ? b ^ 2 : b) & -5;
}
;function sh() {
}
sh.prototype.a = function() {
};
function th(a, b, c) {
  this.b = a;
  this.c = b;
  this.j = c;
}
z(th, sh);
th.prototype.a = function(a, b, c) {
  ph(this.b, this.c, a, b, void 0, c, this.j);
};
function uh(a, b) {
  this.b = a instanceof N ? a : new N(a, b);
}
z(uh, sh);
uh.prototype.a = function(a, b, c, d) {
  ph(Vd(a), 0, a, b, this.b, c, null, d);
};
function vh(a, b) {
  this.Ba = b || void 0;
  of.call(this, a);
}
z(vh, of);
vh.prototype.fb = function() {
  if (this.Ba) {
    var a = !this.Z && "move_offscreen" != this.Ya, b = this.g();
    a && (b.style.visibility = "hidden", R(b, !0));
    this.Ba.a(b, 4, this.La);
    a && R(b, !1);
  }
};
function wh(a, b, c) {
  this.b = c || (a ? O(v(a) ? document.getElementById(a) : a) : O());
  vh.call(this, this.b.D("DIV", {style:"position:absolute;display:none;"}));
  this.h = new N(1, 1);
  this.B = new Zb;
  this.l = null;
  a && xh(this, a);
  null != b && Ad(this.g(), b);
}
z(wh, vh);
var yh = [];
e = wh.prototype;
e.M = null;
e.className = "goog-tooltip";
e.Jc = 0;
function xh(a, b) {
  b = v(b) ? document.getElementById(b) : b;
  a.B.add(b);
  M(b, "mouseover", a.Ec, !1, a);
  M(b, "mouseout", a.yb, !1, a);
  M(b, "mousemove", a.Ua, !1, a);
  M(b, "focus", a.Dc, !1, a);
  M(b, "blur", a.yb, !1, a);
}
e.Sb = function() {
  return this.Jc;
};
e.hc = function(a) {
  var b = this.g();
  b && yd(b);
  wh.m.hc.call(this, a);
  a ? (b = this.b.a.body, b.insertBefore(a, b.lastChild), B(this.l), this.l = new mf(this.g()), ra(this, ia(B, this.l)), M(this.l, "focusin", this.Ma, void 0, this), M(this.l, "focusout", this.lb, void 0, this)) : (B(this.l), this.l = null);
};
function zh(a) {
  return a.j ? a.Z ? 4 : 1 : a.A ? 3 : a.Z ? 2 : 0;
}
e.Cb = function(a) {
  if (!this.Z) {
    return !1;
  }
  var b = Zd(this.g()), c = ae(this.g());
  return b.x <= a.x && a.x <= b.x + c.width && b.y <= a.y && a.y <= b.y + c.height;
};
e.ec = function() {
  if (!of.prototype.ec.call(this)) {
    return !1;
  }
  if (this.a) {
    for (var a, b = 0;a = yh[b];b++) {
      zd(a.g(), this.a) || a.xa(!1);
    }
  }
  Ta(yh, this) || yh.push(this);
  a = this.g();
  a.className = this.className;
  this.Ma();
  M(a, "mouseover", this.ac, !1, this);
  M(a, "mouseout", this.Hc, !1, this);
  Ah(this);
  return !0;
};
e.ib = function() {
  Ua(yh, this);
  for (var a = this.g(), b, c = 0;b = yh[c];c++) {
    b.a && zd(a, b.a) && b.xa(!1);
  }
  this.kc && this.kc.lb();
  Zc(a, "mouseover", this.ac, !1, this);
  Zc(a, "mouseout", this.Hc, !1, this);
  this.a = void 0;
  0 == zh(this) && (this.sa = !1);
  of.prototype.ib.call(this);
};
e.Mc = function(a, b) {
  this.a == a && this.B.contains(this.a) && (this.sa || !this.Yd ? (this.xa(!1), this.Z || (this.a = a, this.Ba = b || Bh(this, 0) || void 0, this.Z && this.fb(), this.xa(!0))) : this.a = void 0);
  this.j = void 0;
};
e.Lc = function(a) {
  this.A = void 0;
  if (a == this.a) {
    a = this.b;
    var b;
    a: {
      var c = a.a;
      try {
        b = c && c.activeElement;
        break a;
      } catch (d) {
      }
      b = null;
    }
    b = b && this.g() && a.contains(this.g(), b);
    null != this.M && (this.M == this.g() || this.B.contains(this.M)) || b || this.w && this.w.M || this.xa(!1);
  }
};
function Ch(a, b) {
  var c = Jd(a.b);
  a.h.x = b.clientX + c.x;
  a.h.y = b.clientY + c.y;
}
e.Ec = function(a) {
  var b = Dh(this, a.target);
  this.M = b;
  this.Ma();
  b != this.a && (this.a = b, this.j || (this.j = kf(y(this.Mc, this, b, void 0), 500)), Eh(this), Ch(this, a));
};
function Dh(a, b) {
  try {
    for (;b && !a.B.contains(b);) {
      b = b.parentNode;
    }
    return b;
  } catch (c) {
    return null;
  }
}
e.Ua = function(a) {
  Ch(this, a);
  this.sa = !0;
};
e.Dc = function(a) {
  this.M = a = Dh(this, a.target);
  this.sa = !0;
  if (this.a != a) {
    this.a = a;
    var b = Bh(this, 1);
    this.Ma();
    this.j || (this.j = kf(y(this.Mc, this, a, b), 500));
    Eh(this);
  }
};
function Bh(a, b) {
  if (0 == b) {
    var c = a.h.clone();
    return new Fh(c);
  }
  return new Gh(a.M);
}
function Eh(a) {
  if (a.a) {
    for (var b, c = 0;b = yh[c];c++) {
      zd(b.g(), a.a) && (b.w = a, a.kc = b);
    }
  }
}
e.yb = function(a) {
  var b = Dh(this, a.target), c = Dh(this, a.h);
  b != c && (b == this.M && (this.M = null), Ah(this), this.sa = !1, !this.Z || a.h && zd(this.g(), a.h) ? this.a = void 0 : this.lb());
};
e.ac = function() {
  var a = this.g();
  this.M != a && (this.Ma(), this.M = a);
};
e.Hc = function(a) {
  var b = this.g();
  this.M != b || a.h && zd(b, a.h) || (this.M = null, this.lb());
};
function Ah(a) {
  a.j && (m.clearTimeout(a.j), a.j = void 0);
}
e.lb = function() {
  2 == zh(this) && (this.A = kf(y(this.Lc, this, this.a), this.Sb()));
};
e.Ma = function() {
  this.A && (m.clearTimeout(this.A), this.A = void 0);
};
e.o = function() {
  var a;
  this.xa(!1);
  Ah(this);
  for (var b = this.B.fa(), c = 0;a = b[c];c++) {
    Zc(a, "mouseover", this.Ec, !1, this), Zc(a, "mouseout", this.yb, !1, this), Zc(a, "mousemove", this.Ua, !1, this), Zc(a, "focus", this.Dc, !1, this), Zc(a, "blur", this.yb, !1, this);
  }
  this.B.clear();
  this.g() && yd(this.g());
  this.M = null;
  delete this.b;
  wh.m.o.call(this);
};
function Fh(a, b) {
  uh.call(this, a, b);
}
z(Fh, uh);
Fh.prototype.a = function(a, b, c) {
  b = Vd(a);
  b = Yd(b);
  c = c ? new Q(c.top + 10, c.right, c.bottom, c.left + 10) : new Q(10, 0, 0, 10);
  rh(this.b, a, 4, c, b, 9) & 496 && rh(this.b, a, 4, c, b, 5);
};
function Gh(a) {
  th.call(this, a, 3);
}
z(Gh, th);
Gh.prototype.a = function(a, b, c) {
  var d = new N(10, 0);
  ph(this.b, this.c, a, b, d, c, 9) & 496 && ph(this.b, 2, a, 1, d, c, 5);
};
function Hh(a, b, c) {
  wh.call(this, a, b, c);
}
z(Hh, wh);
e = Hh.prototype;
e.sc = !1;
e.mb = !1;
e.xb = function() {
  Hh.m.xb.call(this);
  this.S = Nd(ce(this.g()));
  this.a && (this.$ = Nd(ce(this.a)));
  this.mb = this.sc;
  M(this.b.a, "mousemove", this.Ua, !1, this);
};
e.ib = function() {
  Zc(this.b.a, "mousemove", this.Ua, !1, this);
  this.$ = this.S = null;
  this.mb = !1;
  Hh.m.ib.call(this);
};
e.Cb = function(a) {
  if (this.H) {
    var b = Zd(this.g()), c = ae(this.g());
    return b.x - this.H.left <= a.x && a.x <= b.x + c.width + this.H.right && b.y - this.H.top <= a.y && a.y <= b.y + c.height + this.H.bottom;
  }
  return Hh.m.Cb.call(this, a);
};
function Ih(a, b) {
  if (a.$ && a.$.contains(b) || a.Cb(b)) {
    return !0;
  }
  var c = a.w;
  return !!c && c.Cb(b);
}
e.Lc = function(a) {
  this.A = void 0;
  a != this.a || Ih(this, this.h) || this.M || this.w && this.w.M || H && 0 == this.h.x && 0 == this.h.y || this.xa(!1);
};
e.Ua = function(a) {
  var b = this.Z;
  if (this.S) {
    var c = Jd(this.b), c = new N(a.clientX + c.x, a.clientY + c.y);
    Ih(this, c) ? b = !1 : this.mb && (b = Ld(this.S, c) >= Ld(this.S, this.h));
  }
  if (b) {
    if (this.lb(), this.M = null, b = this.w) {
      b.M = null;
    }
  } else {
    3 == zh(this) && this.Ma();
  }
  Hh.m.Ua.call(this, a);
};
e.ac = function() {
  this.M != this.g() && (this.mb = !1, this.M = this.g());
};
e.Sb = function() {
  return this.mb ? 100 : Hh.m.Sb.call(this);
};
function Jh() {
  wh.call(this, void 0, void 0, void 0);
  var a = this.b;
  this.Za = a.a.createElement("PRE");
  this.ka = a.D("BUTTON", null, "Close");
  M(this.ka, "click", y(this.xa, this, !1));
  a = a.D("DIV", null, this.Za, a.a.createElement("HR"), a.D("DIV", {style:"text-align: center;"}, this.ka));
  this.g().appendChild(a);
}
z(Jh, Hh);
Jh.prototype.o = function() {
  ad(this.ka);
  delete this.ka;
  delete this.Za;
  Jh.m.o.call(this);
};
function Kh() {
  T.call(this);
  this.c = new Wg;
  this.ga(this.c);
  this.A = new W(void 0, !0);
  yf(this.A, "Delete session?");
  zf(this.A, Xb("Are you sure you want to delete this session?", null));
  M(this.A, Hf, this.Ba, !1, this);
  this.G = new bh("Delete Session");
  this.ga(this.G);
  M(this.G, "action", y(this.A.X, this.A, !0));
  this.h = new bh("Take Screenshot");
  this.ga(this.h);
  M(this.h, "action", this.La, !1, this);
  this.l = new Jh;
  this.l.H = new Q(5, 5, 5, 5) || null;
  this.l.sc = !0;
  var a = this.l, b = new Q(10, 0, 0, 0);
  null == b || b instanceof Q ? a.La = b : a.La = new Q(b, void 0, void 0, void 0);
  a.Z && a.fb();
  this.l.Jc = 250;
}
z(Kh, T);
Kh.prototype.o = function() {
  this.l.N();
  this.A.N();
  delete this.c;
  delete this.H;
  delete this.I;
  delete this.S;
  delete this.A;
  delete this.l;
  delete this.h;
  delete this.G;
  delete this.$;
  Kh.m.o.call(this);
};
Kh.prototype.K = function() {
  this.h.K();
  this.G.K();
  this.c.K();
  var a = this.a;
  this.H = a.D("DIV", "goog-tab-content empty-view", "No Sessions");
  this.S = a.a.createElement("SPAN");
  this.$ = a.D("DIV", "todo", "\u00a0");
  this.$.disabled = !0;
  this.c.c(this.S);
  var b;
  this.c.c(b = a.D("SPAN", "session-capabilities", "Capabilities"));
  this.c.c(this.h.g());
  this.c.c(this.G.g());
  this.I = a.D("DIV", "goog-tab-content", this.c.g(), this.$);
  this.b = a.D("DIV", null, this.H, this.I, a.D("DIV", "goog-tab-bar-clear"));
  Lh(this, null);
  xh(this.l, b);
};
function Lh(a, b) {
  var c = !!b;
  R(a.H, !c);
  R(a.I, c);
  b && (Ad(a.S, b.O()), a.l.Za.innerHTML = nh(b.a || {}), Mh(b.a, "takesScreenshot") ? (a.h.ea(!0), a.h.oa("")) : (a.h.ea(!1), a.h.oa("Screenshots not supported")));
}
Kh.prototype.Ba = function(a) {
  "ok" == a.key && this.F("delete");
};
Kh.prototype.La = function() {
  this.F("screenshot");
};
function Nh(a) {
  Zg.call(this, "Sessions");
  this.c = new Sg(Qg, null);
  this.l = new Kh;
  this.A = new Yg(a);
  this.H = this.a.D("BUTTON", null, "Create Session");
  this.I = this.a.D("BUTTON", null, "Refresh Sessions");
  this.G = new Wg;
  this.h = [];
  this.$ = setInterval(y(this.Xd, this), 300);
  this.ga(this.c);
  this.ga(this.l);
  this.ga(this.G);
  this.ea(!1);
  this.G.c(this.H);
  this.G.c(this.I);
  M(this.H, "click", y(this.A.X, this.A, !0));
  M(this.I, "click", y(this.F, this, "refresh"));
  M(this.c, "select", this.Md, !1, this);
  M(this.A, "action", this.Gd, !1, this);
}
z(Nh, Zg);
e = Nh.prototype;
e.o = function() {
  ad(this.H);
  ad(this.I);
  clearInterval(this.$);
  this.A.N();
  delete this.A;
  delete this.c;
  delete this.l;
  delete this.G;
  delete this.h;
  delete this.$;
  Nh.m.o.call(this);
};
e.rc = function() {
  this.c.K();
  this.l.K();
  this.G.K();
  return this.a.D("DIV", "session-container", this.G.g(), this.c.g(), this.l.g());
};
e.ea = function(a) {
  a ? (this.H.removeAttribute("disabled"), this.I.removeAttribute("disabled")) : (this.H.setAttribute("disabled", "disabled"), this.I.setAttribute("disabled", "disabled"));
};
function Oh(a) {
  return (a = a.c.Y) ? a.Ha : null;
}
e.Xd = function() {
  if (this.h.length) {
    var a = this.h[0].Ga, a = 5 === a.length ? "." : a + ".";
    D(this.h, function(b) {
      var c = a;
      fg(b.g(), c);
      b.Ga = c;
    });
  }
};
function Ph(a) {
  var b = ae(a.c.g());
  a = a.l;
  b = b.height + 20;
  Od(a.H, "height", b + "px");
  Od(a.I, "height", b + "px");
}
e.mc = function(a) {
  a = new Qh(a);
  var b = this.h.shift(), c = ze(this.c, b);
  0 > c ? this.c.ga(a, !0) : (this.c.ob(a, c, !0), this.c.removeChild(b, !0));
  Ph(this);
  Vg(this.c, a);
};
function Rh(a, b) {
  var c = new gb;
  D(b, function(a) {
    c.na(a.O(), a);
  });
  for (var d = a.c, f = d.Y, g = [], h = we(d) - a.h.length, k = 0;k < h;++k) {
    g.push(V(d, k));
  }
  D(g, function(a) {
    var b = a.Ha.O(), g = jb(c, b);
    g ? (c.remove(b), a.Ha = g) : (d.removeChild(a, !0), f === a && (f = null));
  }, a);
  D(a.h, function(a) {
    d.removeChild(a, !0);
  });
  a.h = [];
  D(c.fa(), a.mc, a);
  f ? (Lh(a.l, f.Ha), Vg(d, f)) : we(d) ? Vg(d, V(d, 0)) : Lh(a.l, null);
}
e.Gd = function() {
  var a = ".";
  this.h.length && (a = this.h[0].Ga);
  a = new vg(a, null, this.a);
  a.ea(!1);
  this.h.push(a);
  this.c.ga(a, !0);
  Ph(this);
  this.F(new Be("create", this, this.A.Tb()));
};
e.Md = function() {
  var a = this.c.Y;
  Lh(this.l, a ? a.Ha : null);
};
function Qh(a) {
  var b = Mh(a.a, "browserName") || "unknown browser", b = b.toLowerCase().replace(/(^|\b)[a-z]/g, function(a) {
    return a.toUpperCase();
  });
  vg.call(this, b);
  this.Ha = a;
}
z(Qh, vg);
Qh.prototype.o = function() {
  delete this.Ha;
  Qh.m.o.call(this);
};
function Sh(a, b) {
  T.call(this, b);
  this.h = a || "";
}
var Th;
z(Sh, T);
e = Sh.prototype;
e.ua = null;
function Uh() {
  null != Th || (Th = "placeholder" in document.createElement("INPUT"));
  return Th;
}
e.Bb = !1;
e.K = function() {
  this.b = this.a.D("INPUT", {type:"text"});
};
e.T = function() {
  Sh.m.T.call(this);
  var a = new le(this);
  a.v(this.g(), "focus", this.Gc);
  a.v(this.g(), "blur", this.kd);
  Uh() ? this.c = a : (H && a.v(this.g(), ["keypress", "keydown", "keyup"], this.pd), a.v(qd(P(this.g())), "load", this.Bd), this.c = a, Vh(this));
  Wh(this);
  this.g().a = this;
};
e.ba = function() {
  Sh.m.ba.call(this);
  this.c && (this.c.N(), this.c = null);
  this.g().a = null;
};
function Vh(a) {
  !a.l && a.c && a.g().form && (a.c.v(a.g().form, "submit", a.qd), a.l = !0);
}
e.o = function() {
  Sh.m.o.call(this);
  this.c && (this.c.N(), this.c = null);
};
e.Gc = function() {
  this.Bb = !0;
  Je(this.g(), "label-input-label");
  if (!Uh() && !Xh(this) && !this.A) {
    var a = this, b = function() {
      a.g() && (a.g().value = "");
    };
    F ? kf(b, 10) : b();
  }
};
e.kd = function() {
  Uh() || (this.c.ia(this.g(), "click", this.Gc), this.ua = null);
  this.Bb = !1;
  Wh(this);
};
e.pd = function(a) {
  27 == a.a && ("keydown" == a.type ? this.ua = this.g().value : "keypress" == a.type ? this.g().value = this.ua : "keyup" == a.type && (this.ua = null), a.j());
};
e.qd = function() {
  Xh(this) || (this.g().value = "", kf(this.jd, 10, this));
};
e.jd = function() {
  Xh(this) || (this.g().value = this.h);
};
e.Bd = function() {
  Wh(this);
};
function Xh(a) {
  return !!a.g() && "" != a.g().value && a.g().value != a.h;
}
e.clear = function() {
  this.g().value = "";
  null != this.ua && (this.ua = "");
};
e.reset = function() {
  Xh(this) && (this.clear(), Wh(this));
};
function Wh(a) {
  var b = a.g();
  Uh() ? a.g().placeholder != a.h && (a.g().placeholder = a.h) : Vh(a);
  Ee(b, "label", a.h);
  Xh(a) ? (b = a.g(), Je(b, "label-input-label")) : (a.A || a.Bb || (b = a.g(), He(b, "label-input-label")), Uh() || kf(a.Sd, 10, a));
}
e.ea = function(a) {
  this.g().disabled = !a;
  var b = this.g();
  a ? Je(b, "label-input-label-disabled") : He(b, "label-input-label-disabled");
};
e.isEnabled = function() {
  return !this.g().disabled;
};
e.Sd = function() {
  !this.g() || Xh(this) || this.Bb || (this.g().value = this.h);
};
function Yh() {
  Xg.call(this, "Open WebDriverJS Script");
  M(this, "show", this.Rd, !1, this);
  this.h = new Sh("Script URL");
  this.ga(this.h);
}
z(Yh, Xg);
e = Yh.prototype;
e.o = function() {
  delete this.h;
  Yh.m.o.call(this);
};
e.qc = function() {
  var a = sd("A", {href:"https://github.com/SeleniumHQ/selenium/wiki/WebDriverJs", target:"_blank"}, "WebDriverJS");
  this.h.K();
  He(this.h.g(), "url-input");
  var b = this.a;
  return b.D("DIV", null, b.D("P", null, "Open a page that has the ", a, " client. The page will be opened with the query parameters required to communicate with the server."), this.h.g());
};
e.Rd = function() {
  this.h.clear();
  this.h.g().focus();
  this.h.g().blur();
};
e.Tb = function() {
  var a = this.h;
  return null != a.ua ? a.ua : Xh(a) ? a.g().value : "";
};
e.Ic = function() {
  return Xh(this.h);
};
function Zh() {
  bh.call(this, "Load Script");
  this.l = new Yh;
  M(this.l, "action", this.I, !1, this);
  M(this, "action", y(this.l.X, this.l, !0));
}
z(Zh, bh);
Zh.prototype.o = function() {
  this.l.N();
  delete this.l;
  Zh.m.o.call(this);
};
Zh.prototype.I = function() {
  this.F(new Be("loadscript", this, this.l.Tb()));
};
function $h(a) {
  this.a = a;
  this.b = {};
}
function ai(a, b, c) {
  a.b[b] = c;
  return a;
}
;function bi() {
}
;function ci(a) {
  this.a = {};
  a && di(this, a);
}
z(ci, bi);
function di(a, b) {
  var c = b instanceof ci ? b.a : b, d;
  for (d in c) {
    if (c.hasOwnProperty(d)) {
      var f = a, g = d, h = c[d];
      null != h ? f.a[g] = h : delete f.a[g];
    }
  }
  return a;
}
function Mh(a, b) {
  var c = null;
  a.a.hasOwnProperty(b) && (c = a.a[b]);
  return null != c ? c : null;
}
;function ei(a, b) {
  this.b = a;
  this.a = di(new ci, b);
}
ei.prototype.O = function() {
  return this.b;
};
ei.prototype.toJSON = function() {
  return this.O();
};
function fi() {
  this.c = {};
}
fi.prototype.h = function(a, b) {
  var c = Array.prototype.slice.call(arguments, 1), d = this.c[a];
  if (d) {
    for (var f = 0;f < d.length;) {
      var g = d[f];
      g.Na.apply(g.scope, c);
      d[f] === g && (d[f].Od ? d.splice(f, 1) : f += 1);
    }
  }
};
function gi(a, b) {
  var c = a.c[b];
  c || (c = a.c[b] = []);
  return c;
}
function hi(a, b, c, d) {
  b = gi(a, b);
  for (var f = b.length, g = 0;g < f;++g) {
    if (b[g].Na == c) {
      return a;
    }
  }
  b.push({Na:c, scope:d, Od:!0});
  return a;
}
function ii(a, b, c) {
  return hi(a, b, c, void 0);
}
fi.prototype.Ib = function(a) {
  n(a) ? delete this.c[a] : this.c = {};
  return this;
};
if (!w(Error.captureStackTrace)) {
  try {
    throw Error();
  } catch (ji) {
  }
}
;/*
 Portions of this code are from the Dojo toolkit, received under the
 BSD License:
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
 Neither the name of the Dojo Foundation nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
*/
function ki(a) {
  sa.call(this, a);
  this.name = "CancellationError";
}
z(ki, sa);
function li(a, b) {
  if (a instanceof ki) {
    return a;
  }
  if (b) {
    var c = b;
    a && (c += ": " + a);
    return new ki(c);
  }
  a && (c = a + "");
  return new ki(c);
}
function mi(a) {
  a.prototype.then = a.prototype.then;
  try {
    Object.defineProperty(a.prototype, "$webdriver_Thenable", {value:!0, enumerable:!1});
  } catch (b) {
    a.prototype.$webdriver_Thenable = !0;
  }
}
function ni(a) {
  if (!a) {
    return !1;
  }
  try {
    return !!a.$webdriver_Thenable;
  } catch (b) {
    return !1;
  }
}
function oi(a, b) {
  x(this);
  this.c = b || La() || pi;
  this.b = this.h = null;
  this.a = "pending";
  this.w = this.l = !1;
  this.j = void 0;
  try {
    var c = this;
    a(function(a) {
      qi(c, "fulfilled", a);
    }, function(a) {
      qi(c, "rejected", a);
    });
  } catch (d) {
    qi(this, "rejected", d);
  }
}
mi(oi);
e = oi.prototype;
e.toString = function() {
  return "Promise::" + x(this) + ' {[[PromiseStatus]]: "' + this.a + '"}';
};
function qi(a, b, c) {
  if ("pending" === a.a) {
    if (c === a) {
      throw new TypeError("A promise may not resolve to itself");
    }
    a.h = null;
    a.a = "blocked";
    if (ni(c)) {
      c.then(a.Kb.bind(a, "fulfilled"), a.Kb.bind(a, "rejected"));
    } else {
      if (da(c)) {
        try {
          var d = c.then;
        } catch (f) {
          a.a = "rejected";
          a.j = f;
          ri(a);
          return;
        }
        if ("function" === typeof d) {
          si(a, c, d);
          return;
        }
      }
      a.a = b;
      a.j = c;
      ri(a);
    }
  }
}
function si(a, b, c) {
  function d(b) {
    g || (g = !0, a.Kb("rejected", b));
  }
  function f(b) {
    g || (g = !0, a.Kb("fulfilled", b));
  }
  var g = !1;
  try {
    c.call(b, f, d);
  } catch (h) {
    d(h);
  }
}
e.Kb = function(a, b) {
  "blocked" === this.a && (this.a = "pending", qi(this, a, b));
};
function ri(a) {
  if (!a.w) {
    a.w = !0;
    ti(a.c);
    var b;
    a.l || "rejected" !== a.a || a.j instanceof ki || (b = ui(a.c), b.Sc = !0);
    a.b && a.b.length && (b = vi(a.c), a.b.forEach(function(a) {
      a.a.a || wi(b, a.a);
    }));
    df(y(a.Dd, a, b));
  }
}
e.Dd = function() {
  var a = this.c;
  --a.w;
  !a.w && a.a && a.kb();
  this.w = !1;
  if (!(this.l || "rejected" !== this.a || this.j instanceof ki)) {
    var a = this.c, b = this.j;
    if (a.a) {
      var c = a.a.a;
      c && c.removeChild(a.a);
      var d = a.a;
      a.a = c;
      d.w = li(b, "Task discarded due to a previous task failure");
      xi(d, d.w);
      d.l || d.h("error", b);
    } else {
      a.lc(b);
    }
  }
  this.b && (a = this.b, this.b = null, a.forEach(this.Ed, this));
};
e.Ed = function(a) {
  var b = this.j, c = a.b, d = a.j;
  "rejected" === this.a && (c = a.l, d = a.h);
  a.a.l = !1;
  w(c) ? yi(a.a.ra, a.a, y(c, void 0, b), a.j, a.h) : (a.a.a && a.a.a.removeChild(a.a), d(b));
};
e.cancel = function(a) {
  this.hb() && (this.h ? this.h.cancel(a) : qi(this, "rejected", li(a)));
};
e.hb = function() {
  return "pending" === this.a;
};
e.then = function(a, b) {
  return zi(this, a, b);
};
e.Aa = function(a) {
  return zi(this, null, a);
};
function zi(a, b, c) {
  if (!w(b) && !w(c)) {
    return a;
  }
  a.l = !0;
  b = new Ai(a, b, c);
  a.b || (a.b = []);
  a.b.push(b);
  "pending" !== a.a && "blocked" !== a.a && (c = a.c, c = c.A || ui(c), wi(c, b.a), ri(a));
  return b.c;
}
function Bi(a) {
  function b(a) {
    if (a === f) {
      throw new TypeError("May not resolve a Deferred with itself");
    }
  }
  var c, d;
  this.c = new oi(function(a, b) {
    c = a;
    d = b;
  }, a);
  var f = this;
  this.j = function(a) {
    b(a);
    c(a);
  };
  this.h = function(a) {
    b(a);
    d(a);
  };
}
mi(Bi);
Bi.prototype.hb = function() {
  return this.c.hb();
};
Bi.prototype.cancel = function(a) {
  this.c.cancel(a);
};
Bi.prototype.then = function(a, b) {
  return this.c.then(a, b);
};
Bi.prototype.Aa = function(a) {
  return this.c.Aa(a);
};
function Ci(a) {
  return !!a && da(a) && "function" === typeof a.then;
}
function Di(a, b) {
  var c = Ya(arguments, 1);
  return new oi(function(b, f) {
    try {
      c.push(function(a, c) {
        a ? f(a) : b(c);
      }), a.apply(void 0, c);
    } catch (g) {
      f(g);
    }
  });
}
function Ei() {
  this.c = {};
  x(this);
  this.j = this.b = this.l = this.A = this.B = this.a = null;
  this.w = 0;
}
z(Ei, fi);
e = Ei.prototype;
e.toString = function() {
  return Fi(this);
};
e.reset = function() {
  this.A = this.a = null;
  this.h("reset");
  this.Ib();
  Gi(this);
  Hi(this);
};
function Fi(a) {
  function b(a, c) {
    var k = a.toString();
    c && (k = "(pending) " + k);
    a === d && (k = "(active) " + k);
    a === f && (k = "(running) " + k);
    a instanceof Ii ? (a.b && (k += "\n" + b(a.b, !0)), a.C && a.C.forEach(function(c) {
      a.b && a.b.l() === c || (k += "\n" + b(c));
    })) : a.l() && (k += "\n" + b(a.l()));
    return "| " + k.replace(/\n/g, "\n| ");
  }
  var c = "ControlFlow::" + x(a), d = a.a, f = a.B;
  return d ? c + "\n" + b(Ji(d)) : c;
}
function ui(a) {
  Gi(a);
  a.a || (a.a = new Ii(a), hi(a.a, "error", a.lc, a), a.kb());
  return a.a;
}
function vi(a) {
  return a.B || ui(a);
}
e.kb = function() {
  this.b || this.w || !this.a || this.a.b || (this.b = new Ki(this.Td, this));
};
function Hi(a) {
  a.b && (a.b.cancel(), a.b = null);
}
function ti(a) {
  a.w += 1;
  Hi(a);
}
e.Td = function() {
  this.b = null;
  if (!this.w) {
    if (!this.a) {
      Li(this);
    } else {
      if (!this.a.b) {
        var a = Mi(this);
        if (a) {
          var b = this.a, c = y(this.kb, this), d = function(d) {
            b.b = null;
            a.w(null);
            a.j(d);
            c();
          }, f = function(d) {
            b.b = null;
            a.w(null);
            a.h(d);
            c();
          };
          b.b = a;
          var g = new Ii(this);
          a.w(g);
          yi(this, g, a.B, function(a) {
            Ci(a) ? a.then(d, f) : a && da(a) && w(a.cd) ? a.cd(d, f) : d && d(a);
          }, f, !0);
        }
      }
    }
  }
};
function Mi(a) {
  var b = a.a, c;
  b.A = !0;
  c = b.C && b.C[0];
  if (!c) {
    return b.l || b.B || (a.a === b && (a.a = b.a), b.a && b.a.removeChild(b), b.h("close"), a.a ? a.kb() : Li(a)), null;
  }
  if (c instanceof Ii) {
    return a.a = c, Mi(a);
  }
  b.removeChild(c);
  return c.hb() ? c : Mi(a);
}
function yi(a, b, c, d, f, g) {
  function h(a) {
    return (!a.C || !a.C.length) && !a.Sc;
  }
  function k(a) {
    var c = b.a;
    c && (c.removeChild(b), df(function() {
      h(c) && c !== l.a && c.h("close");
    }), l.kb());
    a && xi(b, li(a, "Tasks cancelled due to uncaught error"));
    l.a = r;
  }
  var l = a, r = a.a;
  try {
    a.a === b || b.a || wi(a.a, b);
    g && (a.a = b);
    try {
      a.B = b;
      a.A = b;
      Ma.push(a);
      var p = c();
    } finally {
      Ma.pop(), a.A = null, a.B = b.a;
    }
    b.A = !0;
    if (!h(b) || g && Ci(p)) {
      var u;
      Ci(p) ? (b.B = !0, c = function() {
        b.B = !1;
        u = new Ki(function() {
          h(b) && (k(), d(p));
        });
      }, p.then(c, c)) : ni(p) && p.Aa(q);
      ii(ii(b, "close", function() {
        u && u.cancel();
        h(b) && k();
        d(p);
      }), "error", function(a) {
        u && u.cancel();
        ni(p) && p.hb() && p.cancel(a);
        f(a);
      });
    } else {
      k(), d(p);
    }
  } catch (G) {
    k(G), f(G);
  } finally {
    a.B = null;
  }
}
function Li(a) {
  a.l || (Hi(a), a.l = new Ki(a.Wd, a));
}
e.Wd = function() {
  this.j && (clearInterval(this.j), this.j = null);
  this.l = null;
  this.h("idle");
};
function Gi(a) {
  a.l && (a.l.cancel(), a.l = null);
}
e.lc = function(a) {
  this.a = null;
  Gi(this);
  Hi(this);
  this.j && (clearInterval(this.j), this.j = null);
  gi(this, "uncaughtException").length ? this.h("uncaughtException", a) : af(a);
};
function Ki(a, b) {
  this.a = !1;
  df(function() {
    this.a || a.call(b);
  }, this);
}
Ki.prototype.cancel = function() {
  this.a = !0;
};
function Ii(a) {
  this.c = {};
  x(this);
  this.ra = a;
  this.b = this.j = this.C = this.a = null;
  this.Sc = this.l = this.B = this.A = !1;
  this.w = null;
}
z(Ii, fi);
function Ni(a, b) {
  b instanceof Ii ? xi(b, a) : (b.c.b = null, b.cancel(a));
}
function Ji(a) {
  for (;a.a;) {
    a = a.a;
  }
  return a;
}
function xi(a, b) {
  a.C && a.C.forEach(function(a) {
    Ni(b, a);
  });
}
function wi(a, b) {
  if (a.w) {
    Ni(a.w, b);
  } else {
    if (a.C || (a.C = []), b.a = a, a.A && b instanceof Ii) {
      var c = 0;
      a.j instanceof Ii && (c = a.C.indexOf(a.j), c += a.j.l ? 1 : -1);
      a.C.splice(Math.max(c, 0), 0, b);
      a.j = b;
    } else {
      a.j = b, a.C.push(b);
    }
  }
}
Ii.prototype.removeChild = function(a) {
  var b = this.C.indexOf(a);
  a.a = null;
  this.C.splice(b, 1);
  this.j === a && (this.j = this.C[b - 1] || null);
  this.C.length || (this.C = null);
};
Ii.prototype.toString = function() {
  return "Frame::" + x(this);
};
function Ai(a, b, c) {
  Bi.call(this, a.c);
  this.b = b;
  this.l = c;
  this.a = new Ii(a.c);
  this.a.l = !0;
  this.c.h = a;
}
z(Ai, Bi);
var pi = new Ei, Ma = [];
function Oi(a, b) {
  oa.call(this);
  this.b = pc("remote.ui.Client");
  this.w = new vc;
  wc(this.w, !0);
  this.A = a;
  this.l = b;
  this.c = new Ae;
  this.B = new Pf;
  this.a = new Nh(Pi);
  this.j = new Mf;
  this.h = new Zh;
  M(this.a, "create", this.Hd, !1, this);
  M(this.a, "delete", this.Id, !1, this);
  M(this.a, "refresh", this.Rc, !1, this);
  M(this.a, "screenshot", this.Pd, !1, this);
  M(this.h, "loadscript", this.Kd, !1, this);
}
z(Oi, oa);
var Pi = "android;chrome;firefox;internet explorer;iphone;opera".split(";");
e = Oi.prototype;
e.o = function() {
  this.c.N();
  this.a.N();
  this.j.N();
  this.h.N();
  wc(this.w, !1);
  delete this.b;
  delete this.l;
  delete this.w;
  delete this.a;
  delete this.c;
  delete this.j;
  delete this.h;
  Oi.m.o.call(this);
};
function Qi(a) {
  ue(a.c, void 0);
  a.c.Hb(!1);
  ue(a.a, void 0);
  ue(a.B, void 0);
  ue(a.h, void 0);
  a.a.l.c.c(a.h.g());
  Ri(a).then(y(function() {
    this.a.ea(!0);
    this.Rc();
  }, a));
}
function Si(a, b) {
  a.c.Hb(!1);
  var c = y(a.l.c, a.l, b);
  return Di(c).then(na);
}
function Ti(a, b, c) {
  var d = a.b;
  d && d.log(fc, b + "\n" + c, void 0);
  c = a.c;
  c.a.Vd(c.g(), b);
  c.Jb();
  a.c.Hb(!0);
}
function Ri(a) {
  fd(a.b, "Retrieving server status...");
  return Si(a, new $h("getStatus")).then(y(function(a) {
    var c = a.value || {};
    (a = c.os) && a.name && (a = a.name + (a.version ? " " + a.version : ""));
    c = c.build;
    Qf(this.B, a, c && c.version, c && c.revision);
  }, a));
}
e.Rc = function() {
  fd(this.b, "Refreshing sessions...");
  var a = this;
  Si(this, new $h("getSessions")).then(function(b) {
    b = b.value;
    b = Qa(b, function(a) {
      return new ei(a.id, a.capabilities);
    });
    Rh(a.a, b);
  }).Aa(function(b) {
    Ti(a, "Unable to refresh session list.", b);
  });
};
e.Hd = function(a) {
  fd(this.b, "Creating new session for " + a.data.browserName);
  a = ai(new $h("newSession"), "desiredCapabilities", a.data);
  var b = this;
  Si(this, a).then(function(a) {
    a = new ei(a.sessionId, a.value);
    b.a.mc(a);
  }).Aa(function(a) {
    Ti(b, "Unable to create new session.", a);
    a = b.a;
    var d = a.h.shift();
    d && (a.c.removeChild(d, !0), Ph(a));
  });
};
e.Id = function() {
  var a = Oh(this.a);
  if (a) {
    fd(this.b, "Deleting session: " + a.O());
    var b = ai(new $h("quit"), "sessionId", a.O()), c = this;
    Si(this, b).then(function() {
      for (var b = c.a, f = b.c.Y, g, h = we(b.c), k = 0;k < h;++k) {
        var l = V(b.c, k);
        if (l.Ha.O() == a.O()) {
          g = l;
          break;
        }
      }
      g && (b.c.removeChild(g, !0), g.N(), f == g && we(b.c) ? (b = b.c, Vg(b, V(b, 0))) : Lh(b.l, null));
    }).Aa(function(a) {
      Ti(c, "Unable to delete session.", a);
    });
  } else {
    ed(this.b, "Cannot delete session; no session selected!");
  }
};
e.Kd = function(a) {
  var b = Oh(this.a);
  if (b) {
    a = new Cb(a.data);
    a.b.add("wdsid", b.O());
    a.b.add("wdurl", this.A);
    var c = ai(ai(new $h("get"), "sessionId", b.O()), "url", a.toString());
    fd(this.b, "In session(" + b.O() + "), loading " + a);
    Si(this, c).Aa(y(function(a) {
      Ti(this, "Unable to load URL", a);
    }, this));
  } else {
    ed(this.b, "Cannot load url: " + a.data + "; no session selected!");
  }
};
e.Pd = function() {
  var a = Oh(this.a);
  if (a) {
    fd(this.b, "Taking screenshot: " + a.O());
    a = ai(new $h("screenshot"), "sessionId", a.O());
    Nf(this.j, Of);
    this.j.X(!0);
    var b = this;
    Si(this, a).then(function(a) {
      var d = b.j;
      a = a.value;
      if (d.wa) {
        Nf(d, 1);
        a = "data:image/png;base64," + a;
        var f = d.a;
        a = f.D("A", {href:a, target:"_blank"}, f.D("IMG", {src:a}));
        zf(d, Xb("", null));
        d.Fa().appendChild(a);
        tf(d);
      }
    }).Aa(function(a) {
      b.j.X(!1);
      Ti(b, "Unable to take screenshot.", a);
    });
  } else {
    ed(this.b, "Cannot take screenshot; no session selected!");
  }
};
function Ui(a) {
  this.a = a;
  this.b = {};
  this.j = pc("webdriver.http.Executor");
}
Ui.prototype.c = function(a, b) {
  var c = this.b[a.a] || Vi[a.a];
  if (!c) {
    throw Error("Unrecognized command: " + a.a);
  }
  var d = a.b, f = Wi(c.path, d), g = new Xi(c.method, f, d), h = this.j;
  lc(h, function() {
    return ">>>\n" + g;
  });
  this.a.send(g, function(a, c) {
    var d;
    if (!a) {
      lc(h, function() {
        return "<<<\n" + c;
      });
      try {
        d = Yi(c);
      } catch (f) {
        h.log(gc, "Error parsing response", f), a = f;
      }
    }
    b(a, d);
  });
};
function Wi(a, b) {
  var c = a.match(/\/:(\w+)\b/g);
  if (c) {
    for (var d = 0;d < c.length;++d) {
      var f = c[d].substring(2);
      if (f in b) {
        var g = b[f];
        g && g.ELEMENT && (g = g.ELEMENT);
        a = a.replace(c[d], "/" + g);
        delete b[f];
      } else {
        throw Error("Missing required parameter: " + f);
      }
    }
  }
  return a;
}
function Yi(a) {
  try {
    return JSON.parse(a.a);
  } catch (b) {
  }
  var c = {status:0, value:a.a.replace(/\r\n/g, "\n")};
  199 < a.status && 300 > a.status || (c.status = 404 == a.status ? 9 : 13);
  return c;
}
var Vi = function() {
  function a(a) {
    return c("POST", a);
  }
  function b(a) {
    return c("GET", a);
  }
  function c(a, b) {
    return {method:a, path:b};
  }
  return (new function() {
    var a = {};
    this.put = function(b, c) {
      a[b] = c;
      return this;
    };
    this.dd = function() {
      return a;
    };
  }).put("getStatus", b("/status")).put("newSession", a("/session")).put("getSessions", b("/sessions")).put("getSessionCapabilities", b("/session/:sessionId")).put("quit", c("DELETE", "/session/:sessionId")).put("close", c("DELETE", "/session/:sessionId/window")).put("getCurrentWindowHandle", b("/session/:sessionId/window_handle")).put("getWindowHandles", b("/session/:sessionId/window_handles")).put("getCurrentUrl", b("/session/:sessionId/url")).put("get", a("/session/:sessionId/url")).put("goBack", 
  a("/session/:sessionId/back")).put("goForward", a("/session/:sessionId/forward")).put("refresh", a("/session/:sessionId/refresh")).put("addCookie", a("/session/:sessionId/cookie")).put("getCookies", b("/session/:sessionId/cookie")).put("deleteAllCookies", c("DELETE", "/session/:sessionId/cookie")).put("deleteCookie", c("DELETE", "/session/:sessionId/cookie/:name")).put("findElement", a("/session/:sessionId/element")).put("findElements", a("/session/:sessionId/elements")).put("getActiveElement", 
  a("/session/:sessionId/element/active")).put("findChildElement", a("/session/:sessionId/element/:id/element")).put("findChildElements", a("/session/:sessionId/element/:id/elements")).put("clearElement", a("/session/:sessionId/element/:id/clear")).put("clickElement", a("/session/:sessionId/element/:id/click")).put("sendKeysToElement", a("/session/:sessionId/element/:id/value")).put("submitElement", a("/session/:sessionId/element/:id/submit")).put("getElementText", b("/session/:sessionId/element/:id/text")).put("getElementTagName", 
  b("/session/:sessionId/element/:id/name")).put("isElementSelected", b("/session/:sessionId/element/:id/selected")).put("isElementEnabled", b("/session/:sessionId/element/:id/enabled")).put("isElementDisplayed", b("/session/:sessionId/element/:id/displayed")).put("getElementLocation", b("/session/:sessionId/element/:id/location")).put("getElementSize", b("/session/:sessionId/element/:id/size")).put("getElementAttribute", b("/session/:sessionId/element/:id/attribute/:name")).put("getElementValueOfCssProperty", 
  b("/session/:sessionId/element/:id/css/:propertyName")).put("elementEquals", b("/session/:sessionId/element/:id/equals/:other")).put("switchToWindow", a("/session/:sessionId/window")).put("maximizeWindow", a("/session/:sessionId/window/:windowHandle/maximize")).put("getWindowPosition", b("/session/:sessionId/window/:windowHandle/position")).put("setWindowPosition", a("/session/:sessionId/window/:windowHandle/position")).put("getWindowSize", b("/session/:sessionId/window/:windowHandle/size")).put("setWindowSize", 
  a("/session/:sessionId/window/:windowHandle/size")).put("switchToFrame", a("/session/:sessionId/frame")).put("getPageSource", b("/session/:sessionId/source")).put("getTitle", b("/session/:sessionId/title")).put("executeScript", a("/session/:sessionId/execute")).put("executeAsyncScript", a("/session/:sessionId/execute_async")).put("screenshot", b("/session/:sessionId/screenshot")).put("setTimeout", a("/session/:sessionId/timeouts")).put("setScriptTimeout", a("/session/:sessionId/timeouts/async_script")).put("implicitlyWait", 
  a("/session/:sessionId/timeouts/implicit_wait")).put("mouseMoveTo", a("/session/:sessionId/moveto")).put("mouseClick", a("/session/:sessionId/click")).put("mouseDoubleClick", a("/session/:sessionId/doubleclick")).put("mouseButtonDown", a("/session/:sessionId/buttondown")).put("mouseButtonUp", a("/session/:sessionId/buttonup")).put("mouseMoveTo", a("/session/:sessionId/moveto")).put("sendKeysToActiveElement", a("/session/:sessionId/keys")).put("touchSingleTap", a("/session/:sessionId/touch/click")).put("touchDoubleTap", 
  a("/session/:sessionId/touch/doubleclick")).put("touchDown", a("/session/:sessionId/touch/down")).put("touchUp", a("/session/:sessionId/touch/up")).put("touchMove", a("/session/:sessionId/touch/move")).put("touchScroll", a("/session/:sessionId/touch/scroll")).put("touchLongPress", a("/session/:sessionId/touch/longclick")).put("touchFlick", a("/session/:sessionId/touch/flick")).put("acceptAlert", a("/session/:sessionId/accept_alert")).put("dismissAlert", a("/session/:sessionId/dismiss_alert")).put("getAlertText", 
  b("/session/:sessionId/alert_text")).put("setAlertValue", a("/session/:sessionId/alert_text")).put("getLog", a("/session/:sessionId/log")).put("getAvailableLogTypes", b("/session/:sessionId/log/types")).put("getSessionLogs", a("/logs")).put("uploadFile", a("/session/:sessionId/file")).dd();
}();
function Zi(a) {
  var b = [], c;
  for (c in a) {
    b.push(c + ": " + a[c]);
  }
  return b.join("\n");
}
function Xi(a, b, c) {
  this.method = a;
  this.path = b;
  this.data = c || {};
  this.a = {Accept:"application/json; charset=utf-8"};
}
Xi.prototype.toString = function() {
  return [this.method + " " + this.path + " HTTP/1.1", Zi(this.a), "", JSON.stringify(this.data)].join("\n");
};
function $i(a, b, c) {
  this.status = a;
  this.a = c;
  this.b = {};
  for (var d in b) {
    this.b[d.toLowerCase()] = b[d];
  }
}
function aj(a) {
  var b = {};
  if (a.getAllResponseHeaders) {
    var c = a.getAllResponseHeaders();
    c && (c = c.replace(/\r\n/g, "\n").split("\n"), D(c, function(a) {
      a = a.split(/\s*:\s*/, 2);
      a[0] && (b[a[0]] = a[1] || "");
    }));
  }
  return new $i(a.status || 200, b, a.responseText.replace(/\0/g, ""));
}
$i.prototype.toString = function() {
  var a = Zi(this.b), b = ["HTTP/1.1 " + this.status, a];
  a && b.push("");
  this.a && b.push(this.a);
  return b.join("\n");
};
function bj() {
}
;var cj;
function dj() {
}
z(dj, bj);
function ej() {
  var a;
  a: {
    var b = cj;
    if (!b.a && "undefined" == typeof XMLHttpRequest && "undefined" != typeof ActiveXObject) {
      for (var c = ["MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"], d = 0;d < c.length;d++) {
        var f = c[d];
        try {
          new ActiveXObject(f);
          a = b.a = f;
          break a;
        } catch (g) {
        }
      }
      throw Error("Could not create ActiveXObject. ActiveX might be disabled, or MSXML might not be installed");
    }
    a = b.a;
  }
  return a ? new ActiveXObject(a) : new XMLHttpRequest;
}
cj = new dj;
function fj(a) {
  this.a = a;
}
fj.prototype.send = function(a, b) {
  try {
    var c = ej(), d = this.a + a.path;
    c.open(a.method, d, !0);
    c.onload = function() {
      b(null, aj(c));
    };
    c.onerror = function() {
      b(Error(["Unable to send request: ", a.method, " ", d, "\nOriginal request:\n", a].join("")));
    };
    for (var f in a.a) {
      c.setRequestHeader(f, a.a[f] + "");
    }
    c.send(JSON.stringify(a.data));
  } catch (g) {
    b(g);
  }
};
function gj() {
  var a = window.location, a = [a.protocol, "//", a.host, a.pathname.replace(/\/static\/resource(?:\/[^\/]*)?$/, "")].join(""), b = new Ui(new fj(a));
  Qi(new Oi(a, b));
}
var hj = ["init"], ij = m;
hj[0] in ij || !ij.execScript || ij.execScript("var " + hj[0]);
for (var jj;hj.length && (jj = hj.shift());) {
  !hj.length && n(gj) ? ij[jj] = gj : ij[jj] ? ij = ij[jj] : ij = ij[jj] = {};
}
;
